echo "[SCRIPT] Starting"

# define bincout project directory
BINCOUNT_DIR=$PBS_O_WORKDIR
echo "[SCRIPT] bincount project directory: " $BINCOUNT_DIR

# prepare Intel FPGA DevCloud environment
source /data/intel_fpga/devcloudLoginToolSetup.sh
tools_setup -t A10DS

# add PATH to conda user installation
export PATH=/home/$USER/miniconda3/bin:$PATH

cd $BINCOUNT_DIR
echo '[SCRIPT] pwd:' $(pwd)

# build Intel bitstream image (bincount2_intel.aocx)
echo "[SCRIPT] building Intel bitstream image (pac_a10)"
make -f boards/intel-a10/Makefile pac_a10

# convert to unsigned .aocx
printf "\\n%s\\n" "[SCRIPT] Converting to unsigned .aocx:"
cd $BINCOUNT_DIR/build/pac_a10
rm -f bincount2_intel_unsigned.aocx
rm -f signed_bincount2_intel_unsigned.gbs.gz
rm -f temp_bincount2_intel_unsigned.gbs
rm -f temp_bincount2_intel_unsigned.fpga.bin
printf "Y\\nY\\n" | source $AOCL_BOARD_PACKAGE_ROOT/linux64/libexec/sign_aocx.sh -H openssl_manager -i bincount2_intel.aocx -r NULL -k NULL -o bincount2_intel_unsigned.aocx
cd $BINCOUNT_DIR

# program FPGA (must be done explicitely on DevCloud)
echo "[SCRIPT] programming FPGA image"
aocl program acl0 ./build/pac_a10/bincount2_intel_unsigned.aocx 

aocl diagnose

# run simple test using PyOpenCL
echo "[SCRIPT] sourcing conda environment: bincount-pyopencl"
echo "[SCRIPT] environment should be created before running this script (see guide)"
source activate bincount-pyopencl

echo "[SCRIPT] (Test 1) running single-pipeline on hardware"
PYOPENCL_CTX=0 python src/host/demo_single_intel.py -b pac_a10 -t 4 -n 4500000

echo "[SCRIPT] (Test 2) running demo-pipeline on hardware"
PYOPENCL_CTX=0 python src/host/demo_pipeline_intel.py -b pac_a10 -k 50 -x 3 -n 4500000

conda deactivate

echo "[SCRIPT] DONE"
# Usage:
# source /data/intel_fpga/devcloudLoginToolSetup.sh 
# devcloud_login -b A10PAC 1.2.1 scripts/devcloud-a10.sh
