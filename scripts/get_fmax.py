#!/usr/bin/env python

import subprocess

def get_aocx_freq(aocx_pathname):
    # call aocl binedit
    p = subprocess.Popen("aocl binedit "+aocx_pathname+" print .acl.quartus_report", stdout=subprocess.PIPE, shell=True)

    # talk to aocl binedit
    (output, err) = p.communicate()

    # wait for aocl binedit to terminate
    p_status = p.wait()
    
    if p_status:
        print("WARNING: Error when running aocl binedit", aocx_pathname)
        return float('nan')
    else:	
        output_dic = { line.split(":")[0] : line.split(":")[1] for line in output.decode().split("\n") if line }
        return float( output_dic['Actual clock freq'] )

def main():
    import sys
    if len(sys.argv)<2:
        print("Usage: get_fmax.py bin/azint_demo.aocx")
        exit()
    AOCX_FILENAME = sys.argv[1]
    print(get_aocx_freq(AOCX_FILENAME))

if __name__ == "__main__":
    main()
