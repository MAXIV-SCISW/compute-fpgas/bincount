echo "[SCRIPT] Starting"

# define bincout project directory
BINCOUNT_DIR=$PBS_O_WORKDIR
echo "[SCRIPT] bincount project directory: " $BINCOUNT_DIR

# prepare Intel FPGA DevCloud environment
# installation must be done on the node (because of user limits)
source /data/intel_fpga/devcloudLoginToolSetup.sh

# add PATH to conda user installation
export PATH=/home/$USER/miniconda3/bin:$PATH

cd $BINCOUNT_DIR
echo '[SCRIPT] pwd:' $(pwd)

# create Conda environment
echo "[SCRIPT] creating conda environment"
conda env create -f envs/devcloud-jupdemo-39.yml

# install jupyter kernel
echo "[SCRIPT] sourcing conda environment: bincount-pyopencl"
source activate bincount-jupdemo

echo "[SCRIPT] installing jupyter kernel"
python -m ipykernel install --user --name bincount-jupdemo

conda deactivate

echo "[SCRIPT] DONE"
# Usage:
# source /data/intel_fpga/devcloudLoginToolSetup.sh
# devcloud_login -b A10PAC 1.2.1 scripts/devcloud-install-jupdemo.sh
