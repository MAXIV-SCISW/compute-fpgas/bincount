echo "[SCRIPT] Starting"

# define bincout project directory
BINCOUNT_DIR=$PBS_O_WORKDIR
echo "[SCRIPT] bincount project directory: " $BINCOUNT_DIR

# prepare Intel FPGA DevCloud environment
source /data/intel_fpga/devcloudLoginToolSetup.sh
tools_setup -t A10DS

# add PATH to conda user installation
export PATH=/home/$USER/miniconda3/bin:$PATH

cd $BINCOUNT_DIR
echo '[SCRIPT] pwd:' $(pwd)

# build Intel emulation bitstream image (bincount2_intel.aocx)
echo "[SCRIPT] building Intel emulation bitstream image (em-legacy)"
make -f boards/intel-a10/Makefile em-legacy

# run simple test using PyOpenCL
echo "[SCRIPT] sourcing conda environment: bincount-pyopencl"
echo "[SCRIPT] environment should be created befor running this script (see guide)"
source activate bincount-pyopencl

echo "[SCRIPT] running emulation"
CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 PYOPENCL_CTX=0 python src/host/demo_single_intel.py -b em-legacy

conda deactivate

echo "[SCRIPT] DONE"
# Usage:
# source /data/intel_fpga/devcloudLoginToolSetup.sh 
# devcloud_login -b A10PAC 1.2.1 scripts/devcloud-em.sh
