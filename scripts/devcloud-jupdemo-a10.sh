echo "[SCRIPT] Starting"

# define bincout project directory
BINCOUNT_DIR=$PBS_O_WORKDIR
echo "[SCRIPT] bincount project directory: " $BINCOUNT_DIR

# prepare Intel FPGA DevCloud environment
source /data/intel_fpga/devcloudLoginToolSetup.sh
tools_setup -t A10DS

# add PATH to conda user installation
export PATH=/home/$USER/miniconda3/bin:$PATH

cd $BINCOUNT_DIR
echo '[SCRIPT] pwd:' $(pwd)

# program FPGA (must be done explicitely on DevCloud)
echo "[SCRIPT] programming FPGA image"
echo "[SCRIPT] FPGA image should be created before running this script (see guide)"
aocl program acl0 ./build/pac_a10/bincount2_intel_unsigned.aocx 

# run simple test using PyOpenCL
echo "[SCRIPT] sourcing conda environment: bincount-jupdemo"
echo "[SCRIPT] environment should be created before running this script (see guide)"
source activate bincount-jupdemo

# setting the DATA_DIR
echo "[SCRIPT] setting DATA_DIR - this should be customized"
export DATA_DIR=$BINCOUNT_DIR/data
echo "[SCRIPT] DATA_DIR:" $DATA_DIR

# running notebook on hardware
echo "[SCRIPT] running notebook on hardware"
papermill examples/DyCo2.ipynb DyCo2-result.ipynb -k bincount-jupdemo -p PYOPENCL_CTX 0 -p board_name pac_a10

conda deactivate

echo "[SCRIPT] DONE"
# Usage:
# source /data/intel_fpga/devcloudLoginToolSetup.sh 
# devcloud_login -b A10PAC 1.2.1 walltime=00:30:00 scripts/devcloud-jupdemo-a10.sh
