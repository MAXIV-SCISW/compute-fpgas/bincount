# bincount
[![gitlab bincount](https://badgen.net/badge/icon/gitlab?icon=gitlab&label=bincount)](https://gitlab.com/MAXIV-SCISW/compute-fpgas/bincount)
[![Lastest Release](https://gitlab.com/MAXIV-SCISW/compute-fpgas/bincount/-/badges/release.svg)](https://gitlab.com/MAXIV-SCISW/compute-fpgas/bincount)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![DevCloud](https://img.shields.io/badge/Runs%20on-DevCloud-blue?style=plastic&logo=jupyter)](https://www.intel.com/content/www/us/en/developer/tools/devcloud/fpga/overview.html)

1 [Project Overview](#running-project-on-intel-fpga-devcloud)
1.1 [Getting source](#step-1-getting-source)
1.2 [Creating environment](#step-2-preparing-environment)
1.3 [Emulation](#step-3-emulation)
1.4 [Hardware](#step-4-compilation-for-fpga-and-test-run)

bincount implementation of Azimuthal Integration (AZINT) with FPGAs

## Running project on Intel FPGA DevCloud

[Intel FPGA DevCloud](https://www.intel.com/content/www/us/en/developer/tools/devcloud/fpga/overview.html) can be confortable place to start with the FPGA **bincount** project. Intel Aria10 (`pac_a10`) and Stratix10 (`pac_s10_dc`) compute accelerators are included.

When you can get access to Intel DevCloud, all develpent tools are preinstalled. General [DevCloud OpenCL Usage guidelines](https://github.com/intel/FPGA-Devcloud/tree/master/main/QuickStartGuides/OpenCL_Program_PAC_Quickstart) are quite a valuable information source.

### Step 1: Getting source

Update: It seems that from early 2022 there is no Internet on DevCloud login node but there is Internet on the working nodes. So one has to login into a node and execute `git clone` there.

Get the project source:

```bash
source /data/intel_fpga/devcloudLoginToolSetup.sh
devcloud_login -b A10PAC 1.2.1
git clone https://gitlab.com/MAXIV-SCISW/compute-fpgas/bincount.git
```

Conda will be needed for Python virtual environments later. As there may not be a global conda installation at DevCloud anymore, one may want to create a user specific [Miniconda](https://docs.anaconda.com/free/miniconda/index.html) installation. Still being on the node:

```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -b
rm Miniconda3-latest-Linux-x86_64.sh
exit
```

### Step 2: Preparing environment

The following step must be done after every new login. The source line is initialzing environment on DevCloud, see [documentation](https://github.com/intel/FPGA-Devcloud/tree/master/main/QuickStartGuides/OpenCL_Program_PAC_Quickstart).

```bash
cd bincount
source /data/intel_fpga/devcloudLoginToolSetup.sh
```

[PyOpenCL](https://documen.tician.de/pyopencl/) is the only dependency that needs to be installed. There is a Conda environment in `envs/devcloud-39.yml`. You can install it with a conda command or use a batch script included in the project.

At the DevCloud login-node type:

```bash
devcloud_login -b A10PAC 1.2.1 walltime=00:15:00 scripts/devcloud-install-pyopencl.sh
```

DevCloud software environment is sourced at the first line.
The conda environment installation script `scripts/devcloud-install-pyopencl.sh` is subimtted to a node at the second line.

Job can be monitored e.g. by `qstatus` command. There should be an env called `bincount-pyopencl` in `~/miniconda3/envs/` when the job is finished.

### Step 3: Emulation

Execute emulation. There is a job script for it. Note the conda environment must be installed first.

```bash
devcloud_login -b A10PAC 1.2.1 walltime=00:20:00 scripts/devcloud-em.sh
```

A simple very small data pipeline was launched during the emulation. Successfull pass indicates everything fits together. An ouptut should look like below.

```bash
tail -20  devcloud-em.sh.o32280

[azint-cl] OpenCL will use AOCX binary image: ./build/em-legacy/bincount2_intel.aocx
[azint-cl] exec. time transpose(1): 6.06 ms (32x0.00 Mpix frames)
[azint-cl] exec. time transpose(2): 6.32 ms (32x0.00 Mpix frames)
[azint-cl] exec. time     bincount: 52.99 ms (32x0.00 Mpix frames)
[main] --- OpenCL integration ---
[main] bin[:,0]: [256. 256. 256. ...   0.   0.   0.]
[main] ncs[:,0]: [256. 256. 256. ...   0.   0.   0.]
[main] Test passed (bin):  True
[main] Test passed (ncs):  True
[main] normal exit
Error: Specified kernel was not built for any devices
Error: Specified kernel was not built for any devices
Error: Specified kernel was not built for any devices
[SCRIPT] DONE
```

Kernel build erros for emulation can be ignored.

### Step 4: Compilation for FPGA and test-run

Execute FPGA compilation, sythesis and a test run.

```bash
devcloud_login -b A10PAC 1.2.1 scripts/devcloud-a10.sh
```

The hardware synthesis workflow may take around 3.5 hours.

### Step 5: Install Jupyter environment

More software is needed to execute the *examples/DyCO2.ipynb* notebook. This can be provided with `envs/devcloud-jupdemo-39.yml` and setup in a similar way as *PyOpenCL* in one of the steps earlier.

The following lines will install *bincount-jupdemo* Conda environment and a Jupyter kernel with the same name.

```bash
source /data/intel_fpga/devcloudLoginToolSetup.sh
devcloud_login -b A10PAC 1.2.1 walltime=00:30:00 scripts/devcloud-install-jupdemo.sh
```

### Step 6: Execute Jupyter notebook with papermill

As at the time of writing these guidelines it was not possible to execute Jupyter notebooks on FPGA nodes via the web interface, [papermill](https://papermill.readthedocs.io) can be a suitable option. The following script

```bash
devcloud_login -b A10PAC 1.2.1 walltime=00:30:00 scripts/devcloud-jupdemo-a10.sh
```

will actually execute the notebook using these commands:
```bash
export DATA_DIR=~/bincount/data
papermill examples/DyCo2.ipynb DyCo2-result.ipynb -k bincount-jupdemo -p PYOPENCL_CTX 0 -p board_name pac_a10
```

Note that [hc2040_DyCo2.h5](https://www.dropbox.com/s/lh43penrjg9chit/hc2040_DyCo2.h5) (10 GB) dataset is required to be present in `DATA_DIR` for the *examples/DyCo2.ipynb* notebook.


