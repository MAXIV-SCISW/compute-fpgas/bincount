#!/usr/bin/env python

import numpy as np

# --- a helper function to create aligned numpy arrays ---
def np_aligned_empty(shape, dtype, alignbytes):
    # https://stackoverflow.com/questions/9895787/memory-alignment-for-fast-fft-in-python-using-shared-arrays
    dtype = np.dtype(dtype)
    nbytes = np.prod(shape) * dtype.itemsize
    buf = np.empty(nbytes+alignbytes, dtype=np.uint8)
    start_idx = -buf.ctypes.data % alignbytes
    return buf[start_idx:start_idx + nbytes].view(dtype).reshape(shape)

# --- test data generator ---
def test_data2(test,nbin,ndta,nimg,dtp=np.dtype('uint16'),wtp=np.dtype('uint16'),splitpix=3,alignbytes=64,block_len=128):
    """Returns AZINT BINCOUNT test data for 2 banks"""

    if(test==0):
        # linear model, linear data
        pos = np.arange(0,ndta, dtype=np.uint16); pos[()] = np.arange(0,ndta,dtype=np.uint32) % nbin # int32 range needed before modulo
        dta = np.arange(1,ndta+1, dtype=dtp)
    elif(test==1):
        # modulo 4 positions, i.e. 4 different consecutive positions, linear data
        pos = np.arange(0,ndta, dtype=np.uint16); pos = np.arange(0,ndta,dtype=np.uint32) % 4 # int32 range needed before modulo
        dta = np.arange(1,ndta+1, dtype=dtp);
        if (wtp.itemsize<8) and (ndta>1024) and (nimg>1):
            print('[utils_intel.test_data2] WARNING: Test results may have numerical issues for high data length and lower floating type precision.')
    elif(test==2):
        # 4 equal consecutive positions, linear data
        pos = np.arange(0,ndta, dtype=np.uint16); pos = np.arange(0,ndta,dtype=np.uint32) // 4 # int32 range needed before divide
        dta = np.arange(1,ndta+1, dtype=dtp);
    elif(test==3):
        # all pixels to a single target bin, linear data      
        pos = np.arange(0,ndta, dtype=np.uint16); pos[()] = 1 
        dta = np.arange(1,ndta+1, dtype=dtp);
        if (wtp.itemsize<8) and (ndta>1024) and (nimg>1):
            print('[utils_intel.test_data2] WARNING: Test results may have numerical issues for high data length and lower floating type precision.')
    elif(test==4):
        # random positions, random data
        pos = np.random.randint(0,nbin,(ndta,), dtype=np.uint16)
        dta = np.random.randint(0,10,(ndta,), dtype=np.uint32).astype(dtp)
    elif(test==5):
        # splited pixels, modulo 4 positions, i.e. 4 different consecutive positions, linear data
        pix = np.arange(0,ndta*splitpix, dtype=np.uint32) // splitpix
        pos = np.arange(0,ndta*splitpix, dtype=np.uint16); pos[()] = (np.arange(0,ndta*splitpix,dtype=np.uint32) // splitpix) % 4 # int32 range needed before modulo
        wgt = np.ones(pix.shape, dtype=wtp)/splitpix
        cor = np.ones(pix.shape, dtype=wtp)/splitpix; cor[()] = (np.arange(0,ndta*splitpix,dtype=np.uint32) // splitpix)
        dta = np.arange(1,ndta+1, dtype=dtp)
    elif(test==6):
        # splited pixels, modulo 4 positions, i.e. 4 different consecutive positions, linear data and inverse corrections
        pix = np.arange(0,ndta*splitpix, dtype=np.uint32) // splitpix
        pos = np.arange(0,ndta*splitpix, dtype=np.uint16); pos[()] = (np.arange(0,ndta*splitpix,dtype=np.uint32) // splitpix) % 4 # int32 range needed before modulo
        wgt = np.ones(pix.shape, dtype=wtp)/splitpix
        cor = np.ones(pix.shape, dtype=wtp); cor[()] = 1./(np.arange(splitpix,(ndta+1)*splitpix,dtype=np.uint32) // splitpix)
        dta = np.arange(1,ndta+1, dtype=dtp)
    elif(test==7):
        # TODO::
        pix = np.arange(0,ndta*splitpix, dtype=np.uint32) // splitpix
        pos = np.arange(0,ndta*splitpix, dtype=np.uint16); pos[()] = (np.arange(0,ndta*splitpix,dtype=np.uint32) // splitpix) % 4 # int32 range needed before modulo
        wgt = np.ones(pix.shape, dtype=wtp)/splitpix
        cor = np.ones(pix.shape, dtype=wtp); cor[()] = 1./(np.arange(splitpix,(ndta+1)*splitpix,dtype=np.uint32) // splitpix)
        # randomly select several splitpix-tuplets and remove them
        ind = np.random.randint(0,ndta,ndta//6)*splitpix;
        for i in range(splitpix):
            ind = np.concatenate((ind,ind+i),axis=0)
        pix = np.delete(pix, ind)
        pos = np.delete(pos, ind)
        wgt = np.delete(wgt, ind)
        cor = np.delete(cor, ind)
        print("Note: %d control fields deleted", ind.size)
        # random remove several control elements
        ind = np.random.randint(0,pix.size,pix.size//6)
        pix = np.delete(pix, ind)
        pos = np.delete(pos, ind)
        wgt = np.delete(wgt, ind)
        cor = np.delete(cor, ind)
        print("Note: %d control fields deleted", ind.size)
        # remove all instructions for the last pixel (test of end condition)
        ind = np.argwhere(pix==(ndta-1))
        pix = np.delete(pix, ind)
        pos = np.delete(pos, ind)
        wgt = np.delete(wgt, ind)
        cor = np.delete(cor, ind)
        print("Note: %d control fields deleted", ind.size)
        dta = np.arange(1,ndta+1, dtype=dtp)    
    elif(test==8):
        # splited pixels, using only dta from the first pixel (debug)
        pix = np.arange(0,ndta*splitpix, dtype=np.uint32); pix[()] = 0
        pos = np.arange(0,ndta*splitpix, dtype=np.uint16); pos[()] = (np.arange(0,ndta*splitpix,dtype=np.uint32) // splitpix) % 4 # int32 range needed before modulo
        wgt = np.ones(pix.shape, dtype=wtp)/splitpix
        cor = np.ones(pix.shape, dtype=wtp)/splitpix; cor[()] = (np.arange(0,ndta*splitpix,dtype=np.uint32) // splitpix)
        dta = np.arange(1,ndta+1, dtype=dtp)
    else:
        # load saved configuration (useful for debugging)
        pos = np.load('.bincount_last_pos.npy')
        dta = np.load('.bincount_last_dta.npy')

    np.save('.bincount_last_pos.npy',pos)
    np.save('.bincount_last_dta.npy',dta)

    if 'pix' not in dir():
        pix = np.arange(0,ndta, dtype=np.uint32)
    if 'wgt' not in dir():
        wgt = np.ones(pos.shape, dtype=wtp) #*0.9
    if 'cor' not in dir():
        cor = np.ones(pos.shape, dtype=wtp) #*0.9
    if 'splitpix' not in dir():
        splitpix = 1

    # wrap control data to alligned numpy arrays
    if(alignbytes not in [None,[]]):
        _arr = np_aligned_empty(pos.shape, pos.dtype, alignbytes); _arr[()] = pos; pos = _arr
        _arr = np_aligned_empty(pix.shape, pix.dtype, alignbytes); _arr[()] = pix; pix = _arr
        _arr = np_aligned_empty(wgt.shape, wgt.dtype, alignbytes); _arr[()] = wgt; wgt = _arr
        _arr = np_aligned_empty(cor.shape, cor.dtype, alignbytes); _arr[()] = cor; cor = _arr
        _arr = None   

    # replicate data for different images
    dta = np.tile(dta,nimg//2)
    # multiply data with channel nb % 4 + 1
    for i in range(nimg):
        dta[i*dta.size//(nimg//2):(i+1)*dta.size//(nimg//2)] = dta[i*dta.size//(nimg//2):(i+1)*dta.size//(nimg//2)]*(i % 4 + 1)

    # data for 2 banks
    if(alignbytes not in [None,[]]):
        idtalen = ((ndta + block_len - 1) // block_len) * block_len
        if(idtalen != ndta):
            print("[utils_intel.test_data2] WARNING: Input data length is not aligned to BLOCK_LEN = %d. Extending data length: %d -> %d." % (block_len,ndta,idtalen,))
        dta1 = np_aligned_empty(((nimg//2)*idtalen,), dta.dtype, alignbytes); dta1.reshape(nimg//2,idtalen)[:,ndta:] = 0; dta1.reshape(nimg//2,idtalen)[:,:ndta] = dta.reshape(nimg//2,ndta)
        dta2 = np_aligned_empty(((nimg//2)*idtalen,), dta.dtype, alignbytes); dta2.reshape(nimg//2,idtalen)[:,ndta:] = 0; dta2.reshape(nimg//2,idtalen)[:,:ndta] = 2*dta.reshape(nimg//2,ndta)
    else:
        dta2 = 2*dta
        dta1 = dta
    dta = None

    return pix,pos,wgt,cor,dta1,dta2

# --- reference scipy bincount implementation ---
def azint_bincount_scipy(nbins, dta, pix, pos, wgt, cor, fptp=np.dtype('float32')):
    """A reference bincount implementation, returns arrays of shape (nbins,nframes)"""

    nframes = dta.shape[0]
    idtalen = dta.shape[1]

    from scipy.sparse import csc_matrix

    # create Compressed Sparse Column matrix
    spAw = csc_matrix((wgt, (pos, pix)), shape=(nbins,idtalen), dtype=fptp)
    spA  = csc_matrix((wgt*cor, (pos, pix)), shape=(nbins,idtalen), dtype=fptp)

    # integrate
    bin = spA.dot(dta.T)
    ncs = spAw.dot(np.ones(dta.T.shape,dtype=np.int32))

    return bin, ncs

# --- a helper function to get Intel OpenCL bitstream info ---
def cl_bitstream_info(filename):
    import subprocess
    def filter_line(key):
        p = subprocess.Popen("cat " + filename + " | grep -a '" + key + "'", stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        p_status = p.wait()
        return output.decode('utf-8')
    info = {}
    # Actual clock freq
    line = filter_line('Actual clock freq:')
    info['freq'] = float(line.split(':')[1]) if line not in ['',None] else np.NAN
    return info


