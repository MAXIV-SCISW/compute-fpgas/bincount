#!/usr/bin/env python

# demo_single_intel.py

import numpy as np
import pyopencl as cl
import pyopencl.cltypes
import os, sys, getopt

# -------------------------------------- parse arguments --------------------------------------
board_name = 'em'
NIMGS = 32          # must fit FPGA-image version [BIN_RANK_PIX]
NBINS = 1024        # maximum defined by FPGA-image version [NBINS]
BLOCK_LEN = 4096    # must fit FPGA-image version [BLOCK_LEN]
IDTALEN = 1024      # should be a muliply of value in FPGA-image version [BLOCK_LEN]
test_id = 6
splitpix = 1

try:
    opts, args = getopt.getopt(sys.argv[1:],"hb:i:s:c:t:x:n:",["board=","nimg=","nbins=","block_len=","test=","splitpix=","nlen=",])
except getopt.GetoptError:
    print('demo_single_intel.py -b <board-name> -i <n-images> -s <n-bins> -c <block-length> -t <test-id> -x <split-pixel> -n <data-length>')
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print('demo_single_intel.py -b <board-name> -i <n-images> -s <n-bins> -c <block-length> -t <test-id> -x <split-pixel> -n <data-length>')
        sys.exit()
    elif opt in ("-b", "--board"):
        board_name = arg
    elif opt in ("-i", "--nimg"):
        NIMGS = int(arg)
    elif opt in ("-s", "--nbins"):
        NBINS = int(arg)
    elif opt in ("-c", "--block_len"):
        BLOCK_LEN = int(arg)
    elif opt in ("-t", "--test"):
        test_id = int(arg)
    elif opt in ("-x", "--splitpix"):
        splitpix = int(arg)
    elif opt in ("-n", "--nlen"):
        IDTALEN = int(arg)
print('borad-name:', board_name)
print('n-images:', NIMGS)
print('n-bins:', NBINS)
print('block-length:', BLOCK_LEN)
print('test-type:', test_id)
print('split-pixel:', splitpix)
print('data-length:', IDTALEN)
# ---------------------------------------------------------------------------------------------

AOCX_BIN_PATH = os.path.join('./build', board_name)
AOCX_BIN_NAME = 'bincount2_intel.aocx'

IDTP = np.dtype(cl.cltypes.ushort)  # must fit FPGA-image version [T_DATA_VAL]
FPTP = np.dtype(cl.cltypes.float) # must fit FPGA-image version [T_BINNING_VAL]

# --- single OpenCL AZINT execution  ---
def single_azint_run_cl(nbins, dta, pix, pos, wgt, cor, dtp, fptp):
    """Single batch AZINT integration"""

    import pyopencl as cl
    import os

    # a helper function to create aligned numpy arrays    
    def np_aligned_empty(shape, dtype, alignbytes):
        # https://stackoverflow.com/questions/9895787/memory-alignment-for-fast-fft-in-python-using-shared-arrays
        dtype = np.dtype(dtype)
        nbytes = np.prod(shape) * dtype.itemsize
        buf = np.empty(nbytes+alignbytes, dtype=np.uint8)
        start_idx = -buf.ctypes.data % alignbytes
        return buf[start_idx:start_idx + nbytes].view(dtype).reshape(shape)

    block_len = BLOCK_LEN # must fit FPGA-image version [BLOCK_LEN]
    nimg_per_krn = NIMGS # must fit FPGA-image version [BIN_RANK_PIX]

    idtalen = ((dta.shape[1] + block_len - 1) // block_len) * block_len
    ilen = pos.size

    if(idtalen != dta.shape[1]):
        print("[azint-cl] WARNING: input data length is not aligned to BLOCK_LEN = %d" % (block_len,))

    if(nimg_per_krn != dta.shape[0]):
        print("[azint-cl] WARNING: input data size does not fit number of frames this kernel can process (BIN_RANK_PIX = %d)" % (nimg_per_krn,))

    # --- wrap input data into aligned arrays on host ---

    # data type of control arrays must fit the kernel
    pix_h = np_aligned_empty((ilen,), np.uint32, 64); pix_h[()] = pix 
    pos_h = np_aligned_empty((ilen,), np.uint16, 64); pos_h[()] = pos
    wgt_h = np_aligned_empty((ilen,), fptp     , 64); wgt_h[()] = wgt
    cor_h = np_aligned_empty((ilen,), fptp     , 64); cor_h[()] = cor

    dta1_h = np_aligned_empty(((nimg_per_krn//2) * idtalen,), dtp, 64); dta1_h.reshape((nimg_per_krn//2),idtalen)[:,:dta.shape[1]] = dta[:(nimg_per_krn//2),:]
    dta2_h = np_aligned_empty(((nimg_per_krn//2) * idtalen,), dtp, 64); dta2_h.reshape((nimg_per_krn//2),idtalen)[:,:dta.shape[1]] = dta[(nimg_per_krn//2):,:]

    # output arrays
    bin_h = np_aligned_empty((nbins*nimg_per_krn,), fptp, 64); bin_h[()] = 0
    ncs_h = np_aligned_empty((nbins*nimg_per_krn,), fptp, 64); ncs_h[()] = 0

    # a standard opencl dance:
    # 1) load OpenCL image (source for GPU, binary for FPGA)
    # 2) create context
    # 3) create program
    # 4) get kernels
    # 5) create queue
    # 6) allocate space on device
    # 7) set kernel parameters
    # 7) copy input from the host memory to device
    # 8) enquque kernels
    # 9) copy result from device to host

    # --- load OpenCL source or binary ---
    
    # check emulation mode
    fpga_emulation = os.getenv('CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA')=='1'
    if(fpga_emulation):
        print("[azint-cl] FPGA emulation mode (ignore kernel programming error)")

    # load bitstream kernel
    aocx_filename = os.path.join(AOCX_BIN_PATH, AOCX_BIN_NAME)

    print("[azint-cl] OpenCL will use AOCX binary image: %s" % (aocx_filename,))

    # load bitstream kernel
    with open( aocx_filename, "rb") as fid:
        cl_binary = fid.read()

    # --- create context, program, kernels and queue ---
    
    # create some context
    ctx = cl.create_some_context()

    # OpenCL program
    prg = cl.Program(ctx, ctx.devices, [cl_binary,])

    # compute kernel(s)
    krn_bin = prg.bincount2
    krn_transp1 = prg.restrip2
    krn_transp2 = prg.restrip2

    # create OpenCL event queue
    queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # --- allocate space on device ---
    
    mf = cl.mem_flags
    # specific Intel/Altera memory flags
    amf = {'CL_CHANNEL_1_INTELFPGA': 0x00010000,
           'CL_CHANNEL_2_INTELFPGA': 0x00020000, # 0x00020000
           'CL_MEM_HETEROGENEOUS_INTELFPGA': 0x00080000,
           'CL_ALTERA_TIMESTAMP': 0x00090000 }    

    # enqueue copy can be done only after setting the kernel parameters !
    # Note: do not use mf.ALLOC_HOST_PTR !
    pos_d = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], ilen*np.dtype(np.int16).itemsize)
    wgt_d = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_1_INTELFPGA'], ilen*fptp.itemsize)
    cor_d = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], ilen*fptp.itemsize)
    pix_d = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], ilen*np.dtype(np.uint32).itemsize)

    # Note: do not use mf.ALLOC_HOST_PTR !
    dta1_d  = cl.Buffer(ctx, mf.READ_ONLY  | amf['CL_CHANNEL_1_INTELFPGA'], idtalen*(nimg_per_krn//2)*dtp.itemsize)
    dta2_d  = cl.Buffer(ctx, mf.READ_ONLY  | amf['CL_CHANNEL_2_INTELFPGA'], idtalen*(nimg_per_krn//2)*dtp.itemsize)
    dta1t_d = cl.Buffer(ctx, mf.READ_WRITE | amf['CL_CHANNEL_1_INTELFPGA'], idtalen*(nimg_per_krn//2)*dtp.itemsize)
    dta2t_d = cl.Buffer(ctx, mf.READ_WRITE | amf['CL_CHANNEL_2_INTELFPGA'], idtalen*(nimg_per_krn//2)*dtp.itemsize)     
    bin_d   = cl.Buffer(ctx, mf.WRITE_ONLY | amf['CL_CHANNEL_1_INTELFPGA'], 2*nbins*nimg_per_krn*fptp.itemsize)
    ncs_d   = cl.Buffer(ctx, mf.WRITE_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], 2*nbins*nimg_per_krn*fptp.itemsize)

    # internal kernel timing info    
    timing_h = np_aligned_empty((2,), np.uint64, 64)
    timing_d = cl.Buffer(ctx, mf.WRITE_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], 2*np.dtype(np.uint64).itemsize)

    # --- set kernel parameters ---
    # Note: This must be done before enqueue copy in order to enforce the altera memory flags
    krn_bin.set_args( bin_d, ncs_d, np.uint16(nbins),
                      dta1t_d, dta2t_d, np.uint32(idtalen),
                      pix_d, pos_d, wgt_d, cor_d, np.uint32(ilen),
                      timing_d )

    krn_transp1.set_args(dta1t_d, dta2t_d, dta1_d, dta2_d, np.int32(idtalen), np.uint8(0))
    krn_transp2.set_args(dta1t_d, dta2t_d, dta1_d, dta2_d, np.int32(idtalen), np.uint8(1))

    # --- copy input from the host memory to device ---
    cl.enqueue_copy(queue, pos_d, pos_h, is_blocking=True)
    cl.enqueue_copy(queue, pix_d, pix_h, is_blocking=True)
    cl.enqueue_copy(queue, wgt_d, wgt_h, is_blocking=True)
    cl.enqueue_copy(queue, cor_d, cor_h, is_blocking=True)
    cl.enqueue_copy(queue, dta1_d, dta1_h, is_blocking=True)
    cl.enqueue_copy(queue, dta2_d, dta2_h, is_blocking=True)
    queue.finish()
    
    # --- enqueue kernels ---
    evt_t1 = cl.enqueue_nd_range_kernel(queue, krn_transp1, (1,1,1), (1,1,1)) # single task kernel
    evt_t2 = cl.enqueue_nd_range_kernel(queue, krn_transp2, (1,1,1), (1,1,1), wait_for=[evt_t1,]) # single task kernel
    evt_bin = cl.enqueue_nd_range_kernel(queue, krn_bin, (1,1,1), (1,1,1), wait_for=[evt_t2,]) # single task kernel
    queue.flush()
    cl.wait_for_events([evt_bin,])

    # --- copy result from device to host ---
    cl.enqueue_copy(queue, bin_h, bin_d, is_blocking=True)
    cl.enqueue_copy(queue, ncs_h, ncs_d, is_blocking=True)
    cl.enqueue_copy(queue, timing_h, timing_d, is_blocking=True)

    # print some statistics
    print("[azint-cl] exec. time transpose(1): %.2f ms (%dx%.2f Mpix frames)" % ((evt_t1.profile.end-evt_t1.profile.start)*1e-6,nimg_per_krn,dta.shape[1]*1e-6,))
    print("[azint-cl] exec. time transpose(2): %.2f ms (%dx%.2f Mpix frames)" % ((evt_t2.profile.end-evt_t2.profile.start)*1e-6,nimg_per_krn,dta.shape[1]*1e-6,))
    print("[azint-cl] exec. time     bincount: %.2f ms (%dx%.2f Mpix frames)" % ((evt_bin.profile.end-evt_bin.profile.start)*1e-6,nimg_per_krn,dta.shape[1]*1e-6,))

    # release events
    evt_t1 = None
    evt_t2 = None
    evt_bin = None

    # return result
    return bin_h.reshape(nbins,nimg_per_krn), ncs_h.reshape(nbins,nimg_per_krn)

# --- main function ---
def main():
    """Demo main function"""

    print("[main] start")
    
    # import test data generator and scipy reference azint implementation
    from utils_intel import test_data2, azint_bincount_scipy
    
    idtalen = IDTALEN
    nimgs = NIMGS
    nbins = NBINS

    # test data generator returns 2 sets of data with half nb of images each 
    pix,pos,wgt,cor,dta1,dta2 = test_data2(test=test_id,nbin=nbins,ndta=idtalen,nimg=nimgs,dtp=IDTP,wtp=FPTP,splitpix=splitpix)
    idtalen = dta1.size//(nimgs//2) # update input data lenght in case it was adjusted
    pixp = 0    # pixel to be printed
    print("[main] --- Test data ---")
    print("[main] cols:", pix)
    print("[main] rows:", pos)
    print("[main] cor[:5]:", cor[:5])
    print("[main] wgt[:5]:", wgt[:5])  
    print("[main] dta1[%d,:]:" % (pixp % (nimgs//2),), dta1[idtalen*(pixp % (nimgs//2)):idtalen*(pixp % (nimgs//2)+1)])
    print("[main] dta2[%d,:]:" % (pixp % (nimgs//2),), dta2[idtalen*(pixp % (nimgs//2)):idtalen*(pixp % (nimgs//2)+1)])
    
    # reference integration
    rbin, rncs = azint_bincount_scipy(nbins, np.concatenate((dta1,dta2),axis=0).reshape(nimgs,idtalen), pix, pos, wgt, cor, FPTP) # np.dtype('float64')
    print("[main] --- Reference integration ---")
    print("[main] bin[:,%d]:" % (pixp,), rbin[:,pixp])
    print("[main] ncs[:,%d]:" % (pixp,), rncs[:,pixp])
    
    # OpenCL integration
    clbin, clncs = single_azint_run_cl(nbins, np.concatenate((dta1,dta2),axis=0).reshape(nimgs,idtalen), pix, pos, wgt, cor, dtp=IDTP, fptp=FPTP)
    print("[main] --- OpenCL integration ---")
    print("[main] bin[:,%d]:" % (pixp,), clbin[:,pixp])
    print("[main] ncs[:,%d]:" % (pixp,), clncs[:,pixp])

    test_bin = np.allclose(clbin, rbin)
    test_ncs = np.allclose(clncs, rncs)
    print("[main] Test passed (bin): ", test_bin)
    print("[main] Test passed (ncs): ", test_ncs)

    if not test_bin or not test_ncs:
        lidx = np.logical_and(np.isclose(clbin, rbin), np.isclose(clncs, rncs))
        pixe = np.where(~lidx)[1][0]
        bine = np.where(~lidx)[0][0]
        print("[main] (first error) bin[%d,%d]:" % (pixe,bine,), clbin[bine,pixe], ", ref:", rbin[bine,pixe])
        print("[main] (first error) ncs[%d,%d]:" % (pixe,bine,), clncs[bine,pixe], ", ref:", rncs[bine,pixe])

    print("[main] normal exit")
    
if __name__ == "__main__":
    main()

# ------------------------------------------------------------------------
# Usage: CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 PYOPENCL_CTX='0' python src/host/demo_single_intel.py

