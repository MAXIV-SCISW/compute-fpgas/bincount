#!/usr/bin/env python

import numpy as np
#import pyopencl as cl
#import pyopencl.cltypes
import sys, getopt

from bincount_intel import bincount_intel

# -------------------------------------- parse arguments --------------------------------------
board_name = 'em'
NIMG_PER_KRN = 32   # must fit FPGA-image version [BIN_RANK_PIX]
NBINS = 1024        # maximum defined by FPGA-image version [NBINS]
BLOCK_LEN = 4096    # must fit FPGA-image version [BLOCK_LEN]
IDTALEN = 1024      # should be a muliply of value in FPGA-image version [BLOCK_LEN]
test_id = 6
splitpix = 1
NTASKS = 30

try:
    opts, args = getopt.getopt(sys.argv[1:],"hb:i:s:c:t:x:k:n:",["board=","nimg=","nbins=","block_len=","test=","splitpix=","ntaks=","nlen=",])
except getopt.GetoptError:
    print('demo_single_intel.py -b <board-name> -i <n-images-per-task> -s <n-bins> -c <block-length> -t <test-id> -x <split-pixel> -k <n-tasks> -n <data-length>')
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print('demo_single_intel.py -b <board-name> -i <n-images-per-task> -s <n-bins> -c <block-length> -t <test-id> -x <split-pixel> -k <n-tasks> -n <data-length>')
        sys.exit()
    elif opt in ("-b", "--board"):
        board_name = arg
    elif opt in ("-i", "--nimg"):
        NIMG_PER_KRN = int(arg)
    elif opt in ("-s", "--nbins"):
        NBINS = int(arg)
    elif opt in ("-c", "--block_len"):
        BLOCK_LEN = int(arg)
    elif opt in ("-t", "--test"):
        test_id = int(arg)
    elif opt in ("-x", "--splitpix"):
        splitpix = int(arg)
    elif opt in ("-k", "--ntasks"):
        NTASKS = int(arg)
    elif opt in ("-n", "--nlen"):
        IDTALEN = int(arg)
print('borad-name:', board_name)
print('n-images-per-task:', NIMG_PER_KRN)
print('n-bins:', NBINS)
print('block-length:', BLOCK_LEN)
print('test-type:', test_id)
print('split-pixel:', splitpix)
print('data-length:', IDTALEN)
print('n-tasks:', NTASKS)
# ---------------------------------------------------------------------------------------------

IDTP = np.dtype('uint16')  # must fit FPGA-image version [T_DATA_VAL]
FPTP = np.dtype('float32') # must fit FPGA-image version [T_BINNING_VAL]

# --- main function ---
def main():
    """Demo main function"""

    print("[main] start")
    
    # import test data generator and scipy reference azint implementation
    from utils_intel import test_data2, azint_bincount_scipy
    
    idtalen = IDTALEN
    nimgs = NIMG_PER_KRN
    nbins = NBINS

    # test data generator returns 2 sets of data with half nb of images each 
    pix,pos,wgt,cor,dta1,dta2 = test_data2(test=test_id,nbin=nbins,ndta=idtalen,nimg=nimgs,dtp=IDTP,wtp=FPTP,splitpix=splitpix)
    idtalen = dta1.size//(nimgs//2) # update input data lenght in case it was adjusted
    print("[main] --- Test data ---")
    print("[main] cols:", pix)
    print("[main] rows:", pos)
    print("[main] cor[:5]:", cor[:5])
    print("[main] wgt[:5]:", wgt[:5])  
    print("[main] dta1[0,:]:", dta1[:idtalen])
    
    # reference integration
    rbin, rncs = azint_bincount_scipy(nbins, np.concatenate((dta1,dta2),axis=0).reshape(nimgs,idtalen), pix, pos, wgt, cor, FPTP)
    print("[main] --- Reference integration ---")
    print("[main] bin[:,0]:", rbin[:,0])
    print("[main] ncs[:,0]:", rncs[:,0])

    # OpenCL integration
    ntasks = NTASKS
    nframes = ntasks * nimgs
    # create an azint object
    ai = bincount_intel(board_name=board_name, dtp=IDTP, fptp=FPTP, block_len=BLOCK_LEN, nimg_per_krn=NIMG_PER_KRN)
    # set integration parameters
    ai.set_params(nbins, idtalen, pix, pos, wgt, cor, nframes)
    # update input data lenght in case it was adjusted
    dta1 = np.concatenate((dta1.reshape(nimgs//2,idtalen), np.zeros((nimgs//2,ai.idtalen-idtalen),dtype=dta1.dtype)), axis=1).flatten() # zero padding
    dta2 = np.concatenate((dta2.reshape(nimgs//2,idtalen), np.zeros((nimgs//2,ai.idtalen-idtalen),dtype=dta1.dtype)), axis=1).flatten()
    idtalen = ai.idtalen
    # create data
    # multiplicate input/output data
    from utils_intel import np_aligned_empty
    cldta = np_aligned_empty((ntasks*nimgs*idtalen,), IDTP, 64); cldta[()] = 0
    clbin = np_aligned_empty((ntasks*nimgs*nbins,), FPTP, 64); clbin[()] = 0
    clncs = np_aligned_empty((ntasks*nimgs*nbins,), FPTP, 64); clbin[()] = 0
    for itask in range(ntasks):
        i0 = itask * nimgs * idtalen
        i1 = i0 + (nimgs//2) * idtalen
        i2 = i0 + nimgs * idtalen
        cldta[i0:i1] = dta1 * ((itask % 2) + 1) 
        cldta[i1:i2] = dta2 * ((itask % 2) + 1)
    # integrate
    evt = ai.enqueue(clbin, clncs, cldta, nframes)
    ai.wait_for_events([evt,]) # not needed but a good practice
    evt = None # release Event object
    # print some performance statistics
    ai.print_stats()
    ai.release_events() # release all Event objects
    # reshape
    clbin = clbin.reshape(ntasks,nbins,nimgs)
    clncs = clncs.reshape(ntasks,nbins,nimgs)

    print("[main] --- OpenCL integration ---")
    print("[main] bin[:,0]:", clbin[0,:,0])
    print("[main] ncs[:,0]:", clncs[0,:,0])

    test_bin = np.allclose(clbin[0,:,:], rbin, rtol=1e-3)
    test_ncs = np.allclose(clncs[0,:,:], rncs)
    print("[main] Test passed (bin): ", test_bin, "(note: there may be inprecise or overfloating values for higher images)")
    print("[main] Test passed (ncs): ", test_ncs), 

    if not test_bin or not test_ncs:
        lidx = np.logical_and(np.isclose(clbin[0,:,:], rbin, rtol=1e-3), np.isclose(clncs[0,:,:], rncs))
        pixe = np.where(~lidx)[1][0]
        bine = np.where(~lidx)[0][0]
        print("[main] (first error) bin[%d,%d]:" % (pixe,bine,), clbin[0,bine,pixe], ", ref:", rbin[bine,pixe])
        print("[main] (first error) ncs[%d,%d]:" % (pixe,bine,), clncs[0,bine,pixe], ", ref:", rncs[bine,pixe])

    print("[main] normal exit")
    
if __name__ == "__main__":
    main()

# ------------------------------------------------------------------------
# Usage: CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 PYOPENCL_CTX='0' python src/host/demo_pipeline_intel.py
