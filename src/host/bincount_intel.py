#!/usr/bin/env python

import numpy as np
import pyopencl as cl
import pyopencl.cltypes

NIMG_PER_KRN = 32   # must fit FPGA-image version [BIN_RANK_PIX]
NBINS = 1024        # maximum defined by FPGA-image version [NBINS]
BLOCK_LEN = 4096    # must fit FPGA-image version [BLOCK_LEN]
#IDTALEN = 1024      # should be a muliply of value in FPGA-image version [BLOCK_LEN]

IDTP = np.dtype(cl.cltypes.ushort)  # must fit FPGA-image version [T_DATA_VAL]
FPTP = np.dtype(cl.cltypes.float) # must fit FPGA-image version [T_BINNING_VAL]

AOCX_BIN_PATH_ROOT = './build'
AOCX_BIN_NAME = 'bincount2_intel.aocx'

class bincount_intel:

    pipeline_width = 2

    def __init__(self, board_name, dtp=IDTP, fptp=FPTP, block_len=BLOCK_LEN, nimg_per_krn=NIMG_PER_KRN):
        """Initialize AZINT integration environment"""
        import os
        from utils_intel import cl_bitstream_info

        self.dtp = dtp
        self.fptp = fptp
        self.block_len = block_len
        self.nimg_per_krn = nimg_per_krn

        # --- load OpenCL source or binary ---
        
        # check emulation mode
        fpga_emulation = os.getenv('CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA')=='1'
        if(fpga_emulation):
            print("[azint-cl] FPGA emulation mode (ignore kernel programming error)")

        # load bitstream kernel
        aocx_filename = os.path.join(AOCX_BIN_PATH_ROOT, board_name, AOCX_BIN_NAME)

        print("[azint-cl] OpenCL will use AOCX binary image: %s" % (aocx_filename,))

        # load bitstream kernel
        with open( aocx_filename, "rb") as fid:
            cl_binary = fid.read()

        # get bitstream info
        self.bitstream_info = cl_bitstream_info(aocx_filename)

        # --- create context, program, kernels and queue ---
        
        # create some context
        ctx = cl.create_some_context()

        # OpenCL program
        prg = cl.Program(ctx, ctx.devices, [cl_binary,])

        pipeline_width = self.pipeline_width

        # compute kernel(s)
        self.krn_bin = [None]*pipeline_width
        self.krn_t1 = [None]*pipeline_width
        self.krn_t2 = [None]*pipeline_width
        for i in range(pipeline_width):
            self.krn_bin[i%pipeline_width] = prg.bincount2
            self.krn_t1[i%pipeline_width] = prg.restrip2
            self.krn_t2[i%pipeline_width] = prg.restrip2

        # create OpenCL event queue(s)
        # AOCL 17.1 framework is OpenLC ver 1.0 and does not support Out-Of-Order execution
        # so we have to use multiple queues
        # create OpenCL event queue(s)
        self.writeQ1 = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
        self.writeQ2 = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
        self.kt1Q = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
        self.kt2Q = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
        self.kbinQ = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
        self.readQ = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

        self.ctx = ctx
        self.prg = prg

        self.release_events()

    def __del__(self):
        # destructor
        self.release_events()
        # release device memory
        #TODO::

    def set_params(self, nbins, ndta, pix, pos, wgt, cor, nframes, dta = None):
        """Set BINCOUNT control data (i.e. the sparse-matrix)"""

        # nuber of OpenCL tasks
        ntasks = (nframes + (self.nimg_per_krn-1)) // self.nimg_per_krn
        
        # allocate memory on device

        from utils_intel import np_aligned_empty

        block_len = self.block_len
        nimg_per_krn = self.nimg_per_krn
        pipeline_width = self.pipeline_width
        ctx = self.ctx

        self.nbins = nbins
        self.ilen = pos.size
        self.idtalen = ((ndta + block_len - 1) // block_len) * block_len
        if(self.idtalen != ndta):
            print("[azint-cl] WARNING: input data length is not aligned to BLOCK_LEN = %d. Extending data length: %d -> %d." % (block_len, ndta, self.idtalen,))

        ilen = self.ilen
        idtalen = self.idtalen

        mf = cl.mem_flags
        # specific Intel/Altera memory flags
        amf = {'CL_CHANNEL_1_INTELFPGA': 0x00010000, # 0x00010000
               'CL_CHANNEL_2_INTELFPGA': 0x00020000, # 0x00020000
               'CL_MEM_HETEROGENEOUS_INTELFPGA': 0x00080000,
               'CL_ALTERA_TIMESTAMP': 0x00090000 }          

        self.pos_d = [None]*pipeline_width
        self.wgt_d = [None]*pipeline_width
        self.cor_d = [None]*pipeline_width
        self.pix_d = [None]*pipeline_width   

        # enqueue copy can be done only after setting the kernel parameters
        # Note: do not use mf.ALLOC_HOST_PTR !
        for i in range(pipeline_width): #2122
            self.pos_d[i] = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], ilen*np.dtype(np.int16).itemsize)
            self.wgt_d[i] = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_1_INTELFPGA'], ilen*self.fptp.itemsize)
            self.cor_d[i] = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], ilen*self.fptp.itemsize)
            self.pix_d[i] = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], ilen*np.dtype(np.uint32).itemsize)

        self.dta1_d  = [None]*pipeline_width
        self.dta2_d  = [None]*pipeline_width
        self.dta1t_d = [None]*pipeline_width
        self.dta2t_d = [None]*pipeline_width
        self.bin_d   = [None]*pipeline_width
        self.ncs_d   = [None]*pipeline_width

        # Note: do not use mf.ALLOC_HOST_PTR !
        for i in range(pipeline_width):
            self.dta1_d[i]  = cl.Buffer(ctx, mf.READ_ONLY  | amf['CL_CHANNEL_1_INTELFPGA'], idtalen*(nimg_per_krn//2)*self.dtp.itemsize)
            self.dta2_d[i]  = cl.Buffer(ctx, mf.READ_ONLY  | amf['CL_CHANNEL_2_INTELFPGA'], idtalen*(nimg_per_krn//2)*self.dtp.itemsize)
            self.dta1t_d[i] = cl.Buffer(ctx, mf.READ_WRITE | amf['CL_CHANNEL_1_INTELFPGA'], idtalen*(nimg_per_krn//2)*self.dtp.itemsize)
            self.dta2t_d[i] = cl.Buffer(ctx, mf.READ_WRITE | amf['CL_CHANNEL_2_INTELFPGA'], idtalen*(nimg_per_krn//2)*self.dtp.itemsize)     
            self.bin_d[i]   = cl.Buffer(ctx, mf.WRITE_ONLY | amf['CL_CHANNEL_1_INTELFPGA'], nbins*nimg_per_krn*self.fptp.itemsize)
            self.ncs_d[i]   = cl.Buffer(ctx, mf.WRITE_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], nbins*nimg_per_krn*self.fptp.itemsize)

        # internal kernel timing info
        self.timing_h = [None]*ntasks
        self.timing_d = [None]*pipeline_width

        for i in range(ntasks):
            self.timing_h[i] = np_aligned_empty((2,) , np.uint64, 64)

        for i in range(pipeline_width):
            self.timing_d[i] = cl.Buffer(ctx, mf.WRITE_ONLY | amf['CL_CHANNEL_2_INTELFPGA'], 2*np.dtype(np.uint64).itemsize)

        # host memory with pad for raw dta "training"
        pix_h = np_aligned_empty((ilen,), np.uint32, 64); pix_h[()] = pix;
        pos_h = np_aligned_empty((ilen,), np.uint16, 64); pos_h[()] = pos;
        wgt_h = np_aligned_empty((ilen,), self.fptp, 64); wgt_h[()] = wgt;
        cor_h = np_aligned_empty((ilen,), self.fptp, 64); cor_h[()] = cor;

        if dta is None:
            # private data that will be later released
            dta_h = np_aligned_empty((ntasks*nimg_per_krn*idtalen,), self.dtp, 64); dta_h[()] = 0
        elif ((dta.size*np.dtype(dta.dtype).itemsize) >= (ntasks*nimg_per_krn*idtalen*self.dtp.itemsize)):
            # use user supplied buffer
            dta_h = dta
        else:
            raise Exception('[azint-cl] ERROR: Size of dta buffer too small. Required %d bytes, actual size %d bytes.'
                            % (ntasks*nimg_per_krn*idtalen*self.dtp.itemsize, dta.size*np.dtype(dta.dtype).itemsize, ))
            

        bin_h = np_aligned_empty((nbins*nimg_per_krn,), self.fptp, 64)
        ncs_h = np_aligned_empty((nbins*nimg_per_krn,), self.fptp, 64)

        # set kernel arguments and copy data from host to device
        for i in range(pipeline_width):
            # Note: this must be done before enqueue_copy
            self.krn_bin[i%pipeline_width].set_args( self.bin_d[i%pipeline_width], self.ncs_d[i%pipeline_width], np.uint16(nbins),
                                                     self.dta1t_d[i%pipeline_width], self.dta2t_d[i%pipeline_width], np.uint32(idtalen),
                                                     self.pix_d[i%pipeline_width], self.pos_d[i%pipeline_width], self.wgt_d[i%pipeline_width], self.cor_d[i%pipeline_width], np.uint32(ilen),
                                                     self.timing_d[i%pipeline_width])
            # ... dta1
            self.krn_t1[i%pipeline_width].set_args(self.dta1t_d[i%pipeline_width], self.dta2t_d[i%pipeline_width],
                                                   self.dta1_d[i%pipeline_width], self.dta2_d[i%pipeline_width],
                                                   np.int32(idtalen), np.uint8(0))
            # a data transfer training
            for itask in range(ntasks):
                if((itask % pipeline_width)!=(i % pipeline_width)):
                    continue
                ii = itask % ntasks
                # index into image buffer
                i0 = ii * idtalen * nimg_per_krn
                i1 = i0 + idtalen * (nimg_per_krn//2)
                i2 = i0 + idtalen * nimg_per_krn
                cl.enqueue_copy(self.writeQ1, self.dta1_d[itask%pipeline_width], dta_h[i0:i1], is_blocking=True)
            cl.enqueue_nd_range_kernel(self.kt1Q, self.krn_t1[i%pipeline_width], (1,1,1), (1,1,1))
            self.kt1Q.finish()
            # ... dta2
            self.krn_t2[i%pipeline_width].set_args(self.dta1t_d[i%pipeline_width], self.dta2t_d[i%pipeline_width],
                                                   self.dta1_d[i%pipeline_width], self.dta2_d[i%pipeline_width],
                                                   np.int32(idtalen), np.uint8(1))
            # a data transfer training
            for itask in range(ntasks):
                if((itask % pipeline_width)!=(i % pipeline_width)):
                    continue
                ii = itask % ntasks
                # index into image buffer
                i0 = ii * idtalen * nimg_per_krn
                i1 = i0 + idtalen * (nimg_per_krn//2)
                i2 = i0 + idtalen * nimg_per_krn
                cl.enqueue_copy(self.writeQ2, self.dta2_d[itask%pipeline_width], dta_h[i1:i2], is_blocking=True)
            cl.enqueue_nd_range_kernel(self.kt2Q, self.krn_t2[i%pipeline_width], (1,1,1), (1,1,1))
            self.kt2Q.finish()
            # ... control data
            cl.enqueue_copy(self.writeQ2, self.pos_d[i%pipeline_width], pos_h, is_blocking=True)
            cl.enqueue_copy(self.writeQ1, self.wgt_d[i%pipeline_width], wgt_h, is_blocking=True)
            cl.enqueue_copy(self.writeQ1, self.pix_d[i%pipeline_width], pix_h, is_blocking=True)
            cl.enqueue_copy(self.writeQ2, self.cor_d[i%pipeline_width], cor_h, is_blocking=True)       
            cl.enqueue_nd_range_kernel(self.kbinQ, self.krn_bin[i%pipeline_width], (1,1,1), (1,1,1))
            self.kbinQ.finish()
            cl.enqueue_copy(self.readQ, bin_h[:(nbins*nimg_per_krn)], self.bin_d[i%pipeline_width], is_blocking=True)
            cl.enqueue_copy(self.readQ, ncs_h[:(nbins*nimg_per_krn)], self.ncs_d[i%pipeline_width], is_blocking=True)
            cl.enqueue_copy(self.readQ, self.timing_h[i], self.timing_d[i%pipeline_width], is_blocking=True)

    def enqueue(self, obin, oncs, dta, nframes, is_blocking=True, wait_for=[]):
        """Enqueue integration of a series of nframes"""

        # nuber of OpenCL tasks
        ntasks = (nframes + (self.nimg_per_krn-1)) // self.nimg_per_krn

        wdta1_evt = [None]*ntasks
        wdta2_evt = [None]*ntasks
        kt1_evt = [None]*ntasks
        kt2_evt = [None]*ntasks
        kbin_evt = [None]*ntasks
        rbin_evt = [None]*ntasks
        rncs_evt = [None]*ntasks
        rtim_evt = [None]*ntasks
    
        # see section 11. Strategies for Improving Performance in Your Host Application
        # of Intel FPGA SDK for OpenCL Pro Edition: Best Practices Guide
        # in particular part: 11.2.1. Double Buffered Host Application Utilizing Kernel Invocation Queue 
        # ref: https://www.intel.com/content/www/us/en/programmable/documentation/mwh1391807516407.html#ran1551293458343
        
        nimg_per_krn = self.nimg_per_krn
        pipeline_width = self.pipeline_width

        idtalen = self.idtalen
        nbins = self.nbins

        for i in range(ntasks):
  
            ii = i % ntasks

            # index into image buffer
            i0 = ii * idtalen * nimg_per_krn
            i1 = i0 + idtalen * (nimg_per_krn//2)
            i2 = i0 + idtalen * nimg_per_krn
             
            # index into output buffer
            bi1 =  ii * nbins * nimg_per_krn
            bi2 = bi1 + nbins * nimg_per_krn

            # we have already set arguments for all kernels

            if(i<pipeline_width):
                # write1 (h2d)
                wdta1_evt[i] = cl.enqueue_copy(self.writeQ1, self.dta1_d[i%pipeline_width], dta[i0:i1], is_blocking=False, wait_for=wait_for)
                self.writeQ1.flush()
                self.kbinQ.flush()
                # transpose1 (d2d)
                kt1_evt[i] = cl.enqueue_nd_range_kernel(self.kt1Q, self.krn_t1[i%pipeline_width], (1,1,1), (1,1,1), wait_for=[wdta1_evt[i],])
                self.kt1Q.flush()
                # write2 (h2d)
                wdta2_evt[i] = cl.enqueue_copy(self.writeQ2, self.dta2_d[i%pipeline_width], dta[i1:i2], is_blocking=False, wait_for=wait_for)
                kt2_evt[i] = cl.enqueue_nd_range_kernel(self.kt1Q, self.krn_t2[i%pipeline_width], (1,1,1), (1,1,1), wait_for=[wdta2_evt[i],kt1_evt[i],])
                self.kt1Q.flush()
                # bincount kernel
                kbin_evt[i] = cl.enqueue_nd_range_kernel(self.kbinQ, self.krn_bin[i%pipeline_width], (1,1,1), (1,1,1), wait_for=[kt2_evt[i],])
                #self.kbinQ.flush()
            else:
                # write1 (h2d)
                wdta1_evt[i] = cl.enqueue_copy(self.writeQ1, self.dta1_d[i%pipeline_width], dta[i0:i1], is_blocking=False, wait_for=[kt1_evt[i-pipeline_width],])
                self.writeQ1.flush()
                self.kbinQ.flush()
                # transpose1 (d2d)
                kt1_evt[i] = cl.enqueue_nd_range_kernel(self.kt1Q, self.krn_t1[i%pipeline_width], (1,1,1), (1,1,1), wait_for=[wdta1_evt[i],kt2_evt[i-1],kbin_evt[i-pipeline_width],])
                self.kt1Q.flush()
                # write2 (h2d)
                wdta2_evt[i] = cl.enqueue_copy(self.writeQ2, self.dta2_d[i%pipeline_width], dta[i1:i2], is_blocking=False, wait_for=[kt2_evt[i-pipeline_width],])
                # transpose2 (d2d)
                kt2_evt[i] = cl.enqueue_nd_range_kernel(self.kt1Q, self.krn_t2[i%pipeline_width], (1,1,1), (1,1,1), wait_for=[wdta2_evt[i],kt1_evt[i],kbin_evt[i-pipeline_width],])
                self.kt1Q.flush()                
                # bincount kernel
                kbin_evt[i] = cl.enqueue_nd_range_kernel(self.kbinQ, self.krn_bin[i%pipeline_width], (1,1,1), (1,1,1), wait_for=[kt2_evt[i],rbin_evt[i-pipeline_width],])
                #self.kbinQ.flush()
                #self.writeQ1.flush()
            # read (d2h)
            rtim_evt[i] = cl.enqueue_copy(self.readQ, self.timing_h[i], self.timing_d[i%pipeline_width], is_blocking=False, wait_for=[kbin_evt[i],])
            rbin_evt[i] = cl.enqueue_copy(self.readQ, obin[bi1:bi2], self.bin_d[i%pipeline_width], is_blocking=False, wait_for=[rtim_evt[i],])
            rncs_evt[i] = cl.enqueue_copy(self.readQ, oncs[bi1:bi2], self.ncs_d[i%pipeline_width], is_blocking=False, wait_for=[rbin_evt[i],])
            #self.readQ.flush()

        # store Events
        self.wdta1_evt += wdta1_evt
        self.wdta2_evt += wdta2_evt
        self.kt1_evt += kt1_evt
        self.kt2_evt += kt2_evt
        self.kbin_evt += kbin_evt
        self.rbin_evt += rbin_evt
        self.rncs_evt += rncs_evt
        self.rtim_evt += rtim_evt     

        evt = rncs_evt[-1]
        if(is_blocking):
            cl.wait_for_events([evt,])
        
        # return the last event
        return evt

    def allocate_host_dta(self, nframes):
        """Allocate and initialize memory on host for processing nframes"""
        # nuber of OpenCL tasks
        ntasks = (nframes + (self.nimg_per_krn-1)) // self.nimg_per_krn
        # memory aligned array 
        from utils_intel import np_aligned_empty
        dta = np_aligned_empty((ntasks*self.idtalen*self.nimg_per_krn,), self.dtp, 64); dta1[()] = 0;
        return dta

    def wait_for_events(self, events):
        """ Wrapper for PyOpenCL wait_for_events synchronization function"""
        return cl.wait_for_events(events)

    def release_events(self):
        """ Release all event handles"""
        # release Events
        self.wdta1_evt = []
        self.wdta2_evt = []
        self.kt1_evt = []
        self.kt2_evt = []
        self.kbin_evt = []
        self.rbin_evt = []
        self.rncs_evt = []
        self.rtim_evt = []

    def print_stats(self):
        """Print some performance statistics"""

        ntasks = len(self.rtim_evt)
        wdta1_tm = np.zeros((ntasks,),np.float64)
        wdta2_tm = np.zeros((ntasks,),np.float64)
        kt1_tm  = np.zeros((ntasks,),np.float64)
        kt2_tm  = np.zeros((ntasks,),np.float64)
        kbin_tm  = np.zeros((ntasks,),np.float64)
        t0 = self.wdta1_evt[0].profile.start
        print("[azint-cl]    w1     w2    ktp1   ktp2   kAI          write1              write2            transpose1          transpose2             AZINT-bincount")
        print("[azint-cl]   time   time   time   time   time     start      end      start      end      start      end      start      end      start      end    effect")
        print("[azint-cl]   (ms)   (ms)   (ms)   (ms)   (ms)      (ms)     (ms)       (ms)     (ms)       (ms)     (ms)       (ms)     (ms)       (ms)     (ms)      %")
        for i in range(ntasks):
            wdta1_tm[i] = (self.wdta1_evt[i].profile.end - self.wdta1_evt[i].profile.start)*1e-6
            wdta2_tm[i] = (self.wdta2_evt[i].profile.end - self.wdta2_evt[i].profile.start)*1e-6
            kt1_tm[i]   = (self.kt1_evt[i].profile.end - self.kt1_evt[i].profile.start)*1e-6
            kt2_tm[i]   = (self.kt2_evt[i].profile.end - self.kt2_evt[i].profile.start)*1e-6
            kbin_tm[i]  = (self.kbin_evt[i].profile.end - self.kbin_evt[i].profile.start)*1e-6 if self.kbin_evt[i] not in [None,[]] else 0.

            # event runtime in clocks
            kbin_clocks = kbin_tm[i] * self.bitstream_info['freq']*1e3

            print("[azint-cl] %6.1f %6.1f %6.1f %6.1f %6.1f  %8.2f %8.2f   %8.2f %8.2f   %8.2f %8.2f   %8.2f %8.2f   %8.2f %8.2f  %7.2f" % (
                    wdta1_tm[i], wdta2_tm[i], kt1_tm[i], kt2_tm[i], kbin_tm[i],
                    (self.wdta1_evt[i].profile.start-t0)*1e-6, (self.wdta1_evt[i].profile.end-t0)*1e-6,
                    (self.wdta2_evt[i].profile.start-t0)*1e-6, (self.wdta2_evt[i].profile.end-t0)*1e-6,
                    (self.kt1_evt[i].profile.start-t0)*1e-6, (self.kt1_evt[i].profile.end-t0)*1e-6,
                    (self.kt2_evt[i].profile.start-t0)*1e-6, (self.kt2_evt[i].profile.end-t0)*1e-6,
                    (self.kbin_evt[i].profile.start-t0)*1e-6 if self.kbin_evt[i] not in [None,[]] else 0., (self.kbin_evt[i].profile.end-t0)*1e-6 if self.kbin_evt[i] not in [None,[]] else 0.,
                    self.ilen/kbin_clocks*100.,))
        
        sz = self.idtalen * self.nimg_per_krn * self.dtp.itemsize
        szvpix = self.ilen * self.nimg_per_krn

        start = self.wdta1_evt[0].profile.start # (ns)
        end = self.rncs_evt[-1].profile.end # (ns)
        extm = (end-start) * 1e-9 # (sec)
        
        print("[azint-cl] (task-average) wrate1 = %.1f (MB/s), wrate2 = %.1f (MB/s), trate1 = %.1f (MB/s), trate2 = %.1f (MB/s), vpixrate = %.3f (Gpix/s)" % (0.5*sz/np.mean(wdta1_tm)*1e-3,0.5*sz/np.mean(wdta2_tm)*1e-3,0.5*sz/np.mean(kt1_tm)*1e-3,0.5*sz/np.mean(kt2_tm)*1e-3,szvpix/np.mean(kbin_tm)*1e-6,))
        print("[azint-cl] (full-average) tm = %.3f (sec), ntasks = %d, rate = %.1f (MB/s), vpixrate = %.3f (Gpix/s)" % (extm, ntasks, sz*ntasks/extm/1.e6, szvpix*ntasks/extm/1.e9))

