#!/usr/bin/env python

# restrip2_intel kernel test

import numpy as np
import pyopencl as cl
import pyopencl.cltypes
from threading import Thread
from time import sleep
import sys, os, getopt

dtype_in = np.dtype(cl.cltypes.ushort)
dtype_out = np.dtype(cl.cltypes.float)
dtype_ncs = np.dtype(cl.cltypes.float)
dtype_pos = np.dtype(cl.cltypes.ushort)

def np_aligned_empty(shape, dtype, alignbytes):
    # https://stackoverflow.com/questions/9895787/memory-alignment-for-fast-fft-in-python-using-shared-arrays
    dtype = np.dtype(dtype)
    nbytes = np.prod(shape) * dtype.itemsize
    buf = np.empty(nbytes+alignbytes, dtype=np.uint8)
    start_idx = -buf.ctypes.data % alignbytes
    return buf[start_idx:start_idx + nbytes].view(dtype).reshape(shape)

# --- parse arguments ---
board_name = 'em'
npix = 32
nbins = 1024
plen = 8
nlen = 64
test_id = 0
try:
    opts, args = getopt.getopt(sys.argv[1:],"hb:x:s:t:p:n:",["board=","npix=","nbins=","test=","plen=","nlen="])
except getopt.GetoptError:
    print('bincount2.py -b <board-name> -x <npix> -s <nbins> -t <test-id> -p <pos-length> -n <data-length>')
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print('bincount2.py -b <board-name> -x <npix> -s <nbins> -t <test-id> -p <pos-length> -n <data-length>')
        sys.exit()
    elif opt in ("-b", "--board"):
        board_name = arg
    elif opt in ("-x", "--npix"):
        npix = int(arg)
    elif opt in ("-s", "--nbins"):
        nbins = int(arg)
    elif opt in ("-t", "--test"):
        test_id = int(arg)
    elif opt in ("-p", "--plen"):
        plen = int(arg)
    elif opt in ("-n", "--nlen"):
        nlen = int(arg)
print('borad-name:', board_name)
print('npix:', npix)
print('nbins:', nbins)
print('test-id:', test_id)
print('pos-length:', plen)
print('data-length:', nlen)
# -----------------------

fpga_emulation=os.getenv('CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA')=='1'

# load bitstream kernel
if(fpga_emulation):
    with open("./build/em/bincount2_intel.aocx", "rb") as fid:
        cl_binary = fid.read()
else:
    with open("./build/" + board_name + "/bincount2_intel.aocx", "rb") as fid:
        cl_binary = fid.read()

ctx = cl.create_some_context()
mf = cl.mem_flags

prg = cl.Program(ctx, ctx.devices, [cl_binary,]).build()

def run_test(nn):
    global prg, ctx
    global dtype_in, dtype_out, dtype_pos

    ilen = nn

    # Instantiate a Queue with profiling (timing) enabled
    queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    idta1_c  = np_aligned_empty((ilen*(npix//2),), dtype_in, 64); idta1_c[()] = (np.arange(ilen,dtype=dtype_in)+100*np.arange(npix//2).reshape((-1,1))).flatten('F')
    idta2_c  = np_aligned_empty((ilen*(npix//2),), dtype_in, 64); idta2_c[()] = (np.arange(ilen,dtype=dtype_in)+100*np.arange(npix//2, npix).reshape((-1,1))).flatten('F')
    ipos_c  = np_aligned_empty((ilen,), dtype_pos, 64); ipos_c[()] = nbins-1; ipos_c[0:plen] = np.arange(plen); ipos_c[plen:2*plen] = np.arange(plen); #ipos_c[2*plen:3*plen] = np.arange(plen) # ipos_c[()] = np.arange(ilen,dtype=dtype_in)
    if (test_id==1):    
        ipos_c[:2*plen:2] = -ipos_c[:2*plen:2] # mask every second pixel
    elif (test_id==2):
        ipos_c[()] = 2
    elif (test_id==3):
        ipos_c[()] = np.arange(ilen, dtype=dtype_pos) % plen
    ipix_c = np_aligned_empty((ilen,), np.uint32, 64); ipix_c = np.arange(ilen, dtype=np.uint32)
    iwgt_c = np_aligned_empty((ilen,), dtype_out, 64); iwgt_c = np.ones((ilen,), dtype_out)
    icor_c = np_aligned_empty((ilen,), dtype_out, 64); icor_c = np.ones((ilen,), dtype_out)
    odta_c  = np_aligned_empty((nbins*npix,), dtype_out, 64); odta_c[()] = 0
    oncs_c  = np_aligned_empty((nbins*npix,), dtype_ncs, 64); oncs_c[()] = 0

    idta1_d = cl.Buffer(ctx, mf.READ_ONLY  | 0x00080000 | 0x00000000, idta1_c.size*dtype_in.itemsize) # CL_CHANNEL_1_INTELFPGA=65536 0x00010000
    idta2_d = cl.Buffer(ctx, mf.READ_ONLY  | 0x00080000 | 0x00000000, idta2_c.size*dtype_in.itemsize) # CL_CHANNEL_1_INTELFPGA=65536 0x00010000
    ipos_d =  cl.Buffer(ctx, mf.READ_ONLY  | 0x00080000 | 0x00000000, ipos_c.size*dtype_pos.itemsize)
    ipix_d =  cl.Buffer(ctx, mf.READ_ONLY  | 0x00080000 | 0x00000000, ipix_c.size*np.dtype(np.uint32).itemsize)
    iwgt_d =  cl.Buffer(ctx, mf.READ_ONLY  | 0x00080000 | 0x00000000, iwgt_c.size*dtype_out.itemsize)
    icor_d =  cl.Buffer(ctx, mf.READ_ONLY  | 0x00080000 | 0x00000000, icor_c.size*dtype_out.itemsize)
    odta_d =  cl.Buffer(ctx, mf.WRITE_ONLY | 0x00080000 | 0x00000000, odta_c.size*dtype_out.itemsize) # CL_CHANNEL_2_INTELFPGA=131072 0x00020000, CL_MEM_HETEROGENEOUS_INTELFPGA': 0x00080000
    oncs_d =  cl.Buffer(ctx, mf.WRITE_ONLY | 0x00080000 | 0x00000000, oncs_c.size*dtype_ncs.itemsize) # CL_CHANNEL_2_INTELFPGA=131072 0x00020000, CL_MEM_HETEROGENEOUS_INTELFPGA': 0x00080000
    tm_d =    cl.Buffer(ctx, mf.WRITE_ONLY | 0x00080000 | 0x00000000, 2*np.dtype(cl.cltypes.long).itemsize)

    krn = prg.bincount2
    krn.set_args(odta_d, oncs_d, np.uint16(nbins), idta1_d, idta2_d, np.uint32(ilen), ipix_d, ipos_d, iwgt_d, icor_d, np.uint32(ilen), tm_d)
    cl.enqueue_copy(queue, idta1_d, idta1_c, is_blocking=True)
    cl.enqueue_copy(queue, idta2_d, idta2_c, is_blocking=True)
    cl.enqueue_copy(queue, ipos_d, ipos_c, is_blocking=True)
    cl.enqueue_copy(queue, ipix_d, ipix_c, is_blocking=True)
    cl.enqueue_copy(queue, iwgt_d, iwgt_c, is_blocking=True)
    cl.enqueue_copy(queue, icor_d, icor_c, is_blocking=True)
    evt = cl.enqueue_nd_range_kernel(queue, krn, (1,1,1), (1,1,1))
    cl.wait_for_events([evt,])
    elapsed = 1e-6*(evt.profile.end - evt.profile.start)  # Calculate the time it took to execute the kernel    
    evt = None
    print("python: kernel finished")
    cl.enqueue_copy(queue, odta_c, odta_d, is_blocking=True)
    cl.enqueue_copy(queue, oncs_c, oncs_d, is_blocking=True)
 
    print(idta1_c)
    print(ipos_c)
    print(odta_c.reshape(nbins,npix).transpose()[:,:plen])
    print(odta_c.reshape(nbins,npix).transpose())
    print(oncs_c.reshape(nbins,npix).transpose())

    # reference numpy calculation
    # preallocate
    odta_ref = np.zeros((npix,nbins), dtype=dtype_out)
    oncs_ref = np.zeros((npix,nbins), dtype=dtype_ncs)
    not_masked = ipos_c <= 2**(dtype_pos.itemsize*8-1)
    ipos_c[~not_masked] = -ipos_c[~not_masked]
    for ipix in np.arange(npix//2):
        odta_ref[        ipix,:] = np.bincount(ipos_c,weights=idta1_c[ipix::npix//2]*not_masked,minlength=nbins)
        odta_ref[npix//2+ipix,:] = np.bincount(ipos_c,weights=idta2_c[ipix::npix//2]*not_masked,minlength=nbins)
        oncs_ref[        ipix,:] = np.bincount(ipos_c,weights=not_masked,minlength=nbins)
        oncs_ref[npix//2+ipix,:] = np.bincount(ipos_c,weights=not_masked,minlength=nbins)
    print(odta_ref[:,:plen])
    
    print("Test-dta: ", np.allclose(odta_c.reshape(nbins,npix).transpose(),odta_ref))
    print("Test-ncs: ", np.allclose(oncs_c.reshape(nbins,npix).transpose(),oncs_ref))

run_test(nlen)

# ------------------------------------------------------------------------
# Usage: CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 PYOPENCL_CTX='0' python src/host/tests/bincount2_intel.py -b p520_hpc_m210h -n 64
