#!/usr/bin/env python

# restrip2_intel kernel test

import numpy as np
import pyopencl as cl
import pyopencl.cltypes
from threading import Thread
from time import sleep
import sys, os, getopt

dtype_in = np.dtype(cl.cltypes.ushort)
dtype_out = np.dtype(cl.cltypes.ushort)

def np_aligned_empty(shape, dtype, alignbytes):
    # https://stackoverflow.com/questions/9895787/memory-alignment-for-fast-fft-in-python-using-shared-arrays
    dtype = np.dtype(dtype)
    nbytes = np.prod(shape) * dtype.itemsize
    buf = np.empty(nbytes+alignbytes, dtype=np.uint8)
    start_idx = -buf.ctypes.data % alignbytes
    return buf[start_idx:start_idx + nbytes].view(dtype).reshape(shape)

# --- parse arguments ---
board_name = 'em'
mchoice = 0
strip_sz = 16
blk_len = 4096
nlen = 64
try:
    opts, args = getopt.getopt(sys.argv[1:],"hb:m:s:l:n:",["board=","mchoice=","strip_sz=","block_len=","nlen="])
except getopt.GetoptError:
    print('restrip2.py -b <board-name> -m <mem-choice> -s <strip-size> -l <block-length> -n <data-length>')
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print('restrip2.py -b <board-name> -m <mem-choice> -s <strip-size> -l <block-length> -n <data-length>')
        sys.exit()
    elif opt in ("-b", "--board"):
        board_name = arg
    elif opt in ("-m", "--mchoice"):
        mchoice = int(arg)
    elif opt in ("-s", "--strip_sz"):
        strip_sz = int(arg)
    elif opt in ("-l", "--block_len"):
        blk_len = int(arg)
    elif opt in ("-n", "--nlen"):
        nlen = int(arg)
print('borad-name:', board_name)
print('mem-choice:', mchoice)
print('strip-size:', strip_sz)
print('block-length:', blk_len)
print('data-length:', nlen)
# -----------------------

fpga_emulation=os.getenv('CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA')=='1'

# load bitstream kernel
if(fpga_emulation):
    with open("./build/em/bincount2_intel.aocx", "rb") as fid:
        cl_binary = fid.read()
else:
    with open("./build/" + board_name + "/bincount2_intel.aocx", "rb") as fid:
        cl_binary = fid.read()

ctx = cl.create_some_context()
mf = cl.mem_flags

prg = cl.Program(ctx, ctx.devices, [cl_binary,]).build()

def run_test(nn):
    global prg, ctx
    global dtype_in, dtype_out

    # Instantiate a Queue with profiling (timing) enabled
    queue = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    n = ((nn + blk_len - 1)//blk_len)*blk_len
    if(n != nn):
        print("WARNING: input data length is not aligned to BLOCK LENGTH = %d" % (blk_len,))

    in_c  = np_aligned_empty((n*strip_sz,), dtype_in, 64); in_c[()] = np.tile(np.arange(n,dtype=dtype_in),strip_sz)
    for istrip in range(strip_sz):
        in_c[(istrip*n):((istrip+1)*n)] += istrip*10
        in_c[(istrip*n+nn):(istrip*n+n)] = 0
    out_c = np_aligned_empty((n*strip_sz,), dtype_out, 64);

    in0_d  = cl.Buffer(ctx, mf.READ_ONLY  | 0x00080000 | 0x00000000, in_c.size*dtype_in.itemsize) # CL_CHANNEL_1_INTELFPGA=65536 0x00010000
    in1_d  = cl.Buffer(ctx, mf.READ_ONLY  | 0x00080000 | 0x00000000, in_c.size*dtype_in.itemsize)
    out0_d = cl.Buffer(ctx, mf.WRITE_ONLY | 0x00080000 | 0x00000000, out_c.size*dtype_out.itemsize) # CL_CHANNEL_2_INTELFPGA=131072 0x00020000, CL_MEM_HETEROGENEOUS_INTELFPGA': 0x00080000
    out1_d = cl.Buffer(ctx, mf.WRITE_ONLY | 0x00080000 | 0x00000000, out_c.size*dtype_out.itemsize)

    krn = prg.restrip2
    krn.set_args(out0_d, out1_d, in0_d, in1_d, np.int32(n), np.uint8(mchoice))
    cl.enqueue_copy(queue, in0_d, in_c, is_blocking=True)
    cl.enqueue_copy(queue, in1_d, in_c, is_blocking=True)
    evt = cl.enqueue_nd_range_kernel(queue, krn, (1,1,1), (1,1,1))
    cl.wait_for_events([evt,])
    elapsed = 1e-6*(evt.profile.end - evt.profile.start)  # Calculate the time it took to execute the kernel    
    evt = None
    print("python: kernel finished")
    if (mchoice % 2 == 0):
        cl.enqueue_copy(queue, out_c, out0_d, is_blocking=True)
    else:
        cl.enqueue_copy(queue, out_c, out1_d, is_blocking=True)

    print(in_c.reshape(strip_sz,n).T[:,:] == out_c.reshape(n,strip_sz)[:,:])
    print("Test passed: ", np.allclose(in_c.reshape(strip_sz,n).T, out_c.reshape(n,strip_sz)))
    #idx = np.nonzero(in_c.reshape(strip_sz,n).T - out_c.reshape(n,strip_sz))
    pixrate = n*strip_sz / elapsed / 1.e6
    rate = pixrate * (dtype_in.itemsize + dtype_out.itemsize)/2.
    print("[Stats] time = %.3f (ms), pixrate = %.3f (Gpix/sec), rate = %.3f (GB/s)" % (elapsed,pixrate,rate,))
    print(in_c.reshape(strip_sz,n))
    print(out_c.reshape(n,strip_sz)[:,:])
    
run_test(nlen)

# ------------------------------------------------------------------------
# Usage: CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 PYOPENCL_CTX='0' python src/host/tests/restrip2_intel.py -b p520_hpc_m210h -n 64
