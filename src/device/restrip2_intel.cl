/* 
 * restrip2_intel.cl
 *
 * kernel void restrip2 (global ATTRIBUTE_MEN_BANK2 T_DATA_VAL*                restrict odata0,
 *                       global ATTRIBUTE_MEN_BANK3 T_DATA_VAL*                restrict odata1,
                         global ATTRIBUTE_MEN_BANK0 const volatile T_DATA_VAL* restrict idata0,
                         global ATTRIBUTE_MEN_BANK1 const volatile T_DATA_VAL* restrict idata1,
                         int idatalen,
                         uchar mem_choice)

 * A RESTRIP implementation for 2 blocks with total independent BLOCK_STRIP rows
 * of data type: T_DATA_VAL. RESTRIP is effectively an out-of-place transpose
 * of the input data block.
 * 
 * OUTPUT:
 * odata0    ... (idatalen, BLOCK_STRIP) the first transposed data block
 * odata1    ... (idatalen, BLOCK_STRIP) the second transposed data block
 *
 * INPUT:
 * idata0    ... (BLOCK_STRIP, idatalen) the first input data block
 * idata1    ... (BLOCK_STRIP, idatalen) the second input data block
 * idatalen  ... row length, i.e. number of columns of input data blocks
 * 
 * AUXILIARY:
 * mem_choice ... if 0, tranpose done on idata0 and odata0; if 1, tranpose done on idata1 and odata1
 * 
 * The RESTRIP implementation is:
 *
 * 1. utilizing cached load of input block rows into a local memory
 * 2. the local memory storage is cyclically shifted (i.e. rotated by 45 deg) in order
 *    to optimize local memory I/O ports
 * 3. global memory store is effectively fully continuous
 * 
 *
 * Copyright (C) 2021, Zdenek Matej, MAX IV Laboratory, Lund University
 *
 * Developed with significant contribution from:
 *                         
 * - Kenneth Skovhede, Niels Bohr Institute, Copenhagen University
 *   Kenneth Skovhede, MAX IV Laboratory, Lund University
 * - Carl Johnsen, Niels Bohr Institute, Copenhagen University
 * - Artur Barczyk, MAX IV Laboratory, Lund University   
 * 
 * This is a part of AZINT - project.
 * 
 * The AZINT project is developed as a free software in hope it will be
 * useful. A copy of the License should be enclosed with the project.
 *
 */

/* enable OpenCL extension */
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#ifndef T_DATA_VAL
	#define T_DATA_VAL  ushort
#endif

#define BLOCK_LEN 4096

#if !defined(BLOCK_STRIP) || !defined(BLOCK_STRIP_POW)
	#define BLOCK_STRIP 16
	#define BLOCK_STRIP_POW 4
#endif

#define BLOCK_SZ (BLOCK_LEN*BLOCK_STRIP)
#define BLOCK_STRIP_PER_LEN (BLOCK_LEN/BLOCK_STRIP)

#ifdef _USE_HBM_MEM_ /* using multiple HBM memories */
    #define ATTRIBUTE_MEN_BANK0 __attribute((buffer_location("HBM0")))
    #define ATTRIBUTE_MEN_BANK1 __attribute((buffer_location("HBM1")))
    #define ATTRIBUTE_MEN_BANK2 __attribute((buffer_location("HBM2")))
    #define ATTRIBUTE_MEN_BANK3 __attribute((buffer_location("HBM3")))
#else                /* BLANK is default */
    #define ATTRIBUTE_MEN_BANK0
    #define ATTRIBUTE_MEN_BANK1
    #define ATTRIBUTE_MEN_BANK2
    #define ATTRIBUTE_MEN_BANK3
#endif

void restrip_strip_left_shift(T_DATA_VAL a[BLOCK_STRIP], ushort ishift)
{
    #pragma unroll
    for(ushort i=0; i<BLOCK_STRIP; i++)
        if(i<ishift) {
            T_DATA_VAL t = a[0];
            #pragma unroll
            for(size_t j=0; j<(BLOCK_STRIP-1); j++)
                a[j] = a[j+1];
            a[BLOCK_STRIP-1] = t;
        }
}

void restrip_strip_right_shift(T_DATA_VAL a[BLOCK_STRIP], ushort ishift)
{
    #pragma unroll
    for(ushort i=0; i<BLOCK_STRIP; i++)
        if(i<ishift) {
            T_DATA_VAL t = a[BLOCK_STRIP-1];
            #pragma unroll
            for(size_t j=(BLOCK_STRIP-1); j>0; j--)
                a[j] = a[j-1];
            a[0] = t;
        }
}

kernel __attribute__((max_global_work_dim(0)))
void restrip2(__global ATTRIBUTE_MEN_BANK2       T_DATA_VAL* restrict odata0,
              __global ATTRIBUTE_MEN_BANK3       T_DATA_VAL* restrict odata1,
              __global ATTRIBUTE_MEN_BANK0 const T_DATA_VAL* restrict idata0,
              __global ATTRIBUTE_MEN_BANK1 const T_DATA_VAL* restrict idata1,
              int idatalen,
              uchar mem_choice)
{
    // local mem block
    local T_DATA_VAL __attribute__((numbanks(2*BLOCK_STRIP),
                                    bankwidth(sizeof(T_DATA_VAL)),
                                    singlepump,
                                    numreadports(1),
                                    numwriteports(1))) block[BLOCK_STRIP][BLOCK_LEN][2];

    const uint nblocks = (idatalen+(BLOCK_LEN-1))/BLOCK_LEN;

    #pragma ii 1
    for(uint iblk=0; iblk<(nblocks+1); iblk++) {

        uint index_out = ((iblk>0) ? (iblk-1) : 0) * BLOCK_LEN * BLOCK_STRIP;

        // cycle over strips (frames)
        #pragma unroll 1
        #pragma ivdep 
        for(ushort istrip=0; istrip<BLOCK_STRIP; istrip++) {            

            uint index_in = iblk * BLOCK_LEN + istrip * nblocks * BLOCK_LEN;

            // --- load from global memory ---
            #pragma ii 1
            for(uint iload=0; iload<BLOCK_STRIP_PER_LEN; iload++) {
                const uint offset = iload * BLOCK_STRIP;
                const uint offset_load = index_in + offset;
                T_DATA_VAL __attribute__((register)) row_in[BLOCK_STRIP];
                // load
                #pragma unroll
                for(uint idx=0; idx<BLOCK_STRIP; idx++) // cycle over pixel
                    row_in[idx] = (mem_choice & 1) ? idata1[offset_load + idx] : idata0[offset_load + idx]; // BC-non-aligned LSU
                // right shift
                restrip_strip_right_shift(row_in, istrip);
                // --- store to local memory ---
                #pragma unroll
                for(ushort icol=0; icol<BLOCK_STRIP; icol++) {
                    ushort irow = (icol + BLOCK_STRIP - istrip) & (BLOCK_STRIP-1);
                    block[irow][offset+icol][iblk & 0x1] = row_in[icol];
                }
            }

            // --- make sure local memory operations are finished ---
            mem_fence(CLK_LOCAL_MEM_FENCE);
 
            const bool x = mem_choice & 1;

            // --- store to global memory ---
            //#pragma ii 1
            #pragma unroll 1
            for(uint istore=0; istore<BLOCK_STRIP_PER_LEN; istore++) {
                T_DATA_VAL __attribute__((register)) row_out[BLOCK_STRIP];
                // --- load from local memory ---
                ushort irow = (istrip * BLOCK_STRIP_PER_LEN + istore) & (BLOCK_STRIP-1);
                ushort icol_x = (istrip * BLOCK_STRIP_PER_LEN + istore) >> BLOCK_STRIP_POW;
                #pragma unroll
                for(ushort icol=0; icol<BLOCK_STRIP; icol++) {
                    row_out[icol] = block[irow][icol_x * BLOCK_STRIP + icol][(iblk+1) & 0x1];
                }
                // left shift
                restrip_strip_left_shift(row_out, irow);
                // store
                const uint offset = istore * BLOCK_STRIP;
                const uint offset_store = index_out + istrip*BLOCK_LEN + offset;
                /*if(mem_choice & 1) {
                    #pragma unroll
                    for(uint idx=0; idx<BLOCK_STRIP; idx++) // cycle over pixels
                        odata1[offset_store + idx] = row_out[idx];
                } else {
                    #pragma unroll
                    for(uint idx=0; idx<BLOCK_STRIP; idx++) // cycle over pixels
                        odata0[offset_store + idx] = row_out[idx]; 
                }*/
                #pragma unroll
                for(uint idx=0; idx<BLOCK_STRIP; idx++) { // cycle over pixels
                    if(x)
                        odata1[offset_store + idx] = row_out[idx];
                    else
                        odata0[offset_store + idx] = row_out[idx];
                }
            }

        } // istrip

        // --- make sure local memory operations are finished ---
        mem_fence(CLK_LOCAL_MEM_FENCE);

        /*printf("in-block[%d]:\n", iblk & 0x1);
        for(int i=0; i<BLOCK_STRIP; i++) {
            for(int j=0; j<BLOCK_LEN; j++)
                printf(" %3d", block[i][j][iblk & 0x1]);
            printf("\n");
        }*/

    } // iblk
}

