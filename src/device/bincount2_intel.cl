/* 
 * bincount2_intel.cl
 *
 * kernel void bincount2 ( global ATTRIBUTE_MEN_BANK4 T_BINNING_VAL* restrict odata,
 *                         global ATTRIBUTE_MEN_BANK4 T_BINNING_NCS* restrict oncounts,
 *                         const unsigned short olen,
 *                         global ATTRIBUTE_MEN_BANK2 const T_DATA_VAL* restrict idata1,
 *                         global ATTRIBUTE_MEN_BANK3 const T_DATA_VAL* restrict idata2,
 *                         const unsigned int idatalen,
 *                         global ATTRIBUTE_MEN_BANK4 const unsigned int*   restrict ipixels,
 *                         global ATTRIBUTE_MEN_BANK4 const unsigned short* restrict ipositions,
 *                         global ATTRIBUTE_MEN_BANK4 const T_BINNING_VAL* restrict iweights,
 *                         global ATTRIBUTE_MEN_BANK4 const T_BINNING_VAL* restrict icorrections,
 *                         const unsigned int ilen,
 *                         global ATTRIBUTE_MEN_BANK4 unsigned long *restrict timing )
 *
 * A BIN-COUNTING implementation for 2 blocks with total BIN_RANK_PIX independent series
 * of data of type T_DATA_VAL. Implementation optimized for azimuthal integration of
 * scattering (pixel) data. This means the raw (pixel) data are multiplied by weights
 * and corrections before bincounting and also weights are bincounted intependently for
 * normalization. Positions can address raw (pixel) data multiple times in order to account
 * for so called pixel-splitting. Raw (pixel) data are equaly divided in two blocks in order
 * to allocate them on different memory banks when data access bandwidth needs to be optimized.
 * 
 * OUTPUT:
 * odata        ... (olen, BIN_RANK_PIX) weighted, corrected and bincounted data
 * oncounts     ... (olen, BIN_RANK_PIX) bincounted weights
 * olen         ... number of output bins (max NBINS)
 *
 * RAW (PIXEL) DATA:
 * idata1       ... the first input data block of shape (idatalen, BIN_RANK_PIX/2)
 * idata2       ... the second input data block of shape (idatalen, BIN_RANK_PIX/2)
 * idatalen     ... size of raw (pixel) data, i.e. number of pixels
 * 
 * BINCOUNT DATA
 * ipixels      ... array of size (ilen,) with raw (pixel) data indexes
 * ipositions   ... array of size (ilen,) with (pixel) target bin index (the lowest bit is mask)
 * iweights     ... array of size (ilen,) with weights for each bincounted pixel
 * icorrections ... array of size (ilen,) with corrections for each bincounted pixel
 * ilen         ... number of bincount instructions
 *
 * AUXILIARY:
 * timing       ... (development) some implementations can store timing info here
 * 
 * The BIN-COUNTING implementation is utilizing:
 *
 * 1. relaxation of a loop-carried dependency, due to (floating point) add operation using
 *   strategy of increasing the dependence distance, i.e. loop iterations between usage
 *   (storing) of the accumulated value and the point when a new bin is scheduled to accumulate.
 *   The strategy is described in the section 6.1.2 of Intel FPGA SDK for OpenCL Pro Edition:
 *   Best Practices Guide (v20.4) or the section 5.1.2 in the earlier version 17.1.
 *
 *   [ref] https://www.intel.com/content/dam/altera-www/global/en_US/pdfs/literature/hb/opencl-sdk/aocl-best-practices-guide.pdf
 *
 * 2. bisort & resort pipeline to ensure non-repeatabality of input sequence on a fixed interval. 
 * 
 *
 * Copyright (C) 2021, Zdenek Matej, MAX IV Laboratory, Lund University
 *
 * Developed with significant contribution from:
 *                         
 * - Kenneth Skovhede, Niels Bohr Institute, Copenhagen University
 *   Kenneth Skovhede, MAX IV Laboratory, Lund University
 * - Carl Johnsen, Niels Bohr Institute, Copenhagen University
 * - Artur Barczyk, MAX IV Laboratory, Lund University   
 * 
 * This is a part of AZINT - project.
 * 
 * The AZINT project is developed as a free software in hope it will be
 * useful. A copy of the License should be enclosed with the project.
 *
 */

/* enable OpenCL extensions */
#pragma OPENCL EXTENSION cl_intel_channels : enable
#pragma OPENCL EXTENSION cl_khr_fp64 : enable

#ifndef T_DATA_VAL
	#define T_DATA_VAL  ushort
#endif

#define T_BINNING_VAL   float
#define T_BINNING_NCS   float
#define ULONG_AS_VAL_TYPE(x)  (as_float((uint)(x)))
#define VAL_TYPE_AS_ITYPE(x)  (as_int((x)))
#define ULONG_AS_NCS_TYPE(x)  (as_float((uint)(x)))
#define NCS_TYPE_AS_ITYPE(x)  (as_int((x)))

#include "bisort_prg8.h" /* definition of the bitonic mergesort network */

/* wrapping global net* to net names */
#define BN_N		BISORT_NET8_N
#define BN_LEN	   	BISORT_NET8_LEN
#define BN_DEPTH	BISORT_NET8_DEPTH
#define _bp		 	_bisort_prg8

#define UNSEQ_LEN	BN_N	/* unique sequence length */

#ifndef BIN_RANK_PIX
	#define BIN_RANK_PIX 1 /* (BN_N*BIN_RANK_BISORT) */
#endif

#define BIN_RANK_BISORT	((BIN_RANK_PIX+BN_N-1)/BN_N)  /* number of bisort units */

#define NBINS		8192

#define RELAX_LEN	7

/* the largest int16 value is used as a sink position */
#define PIX_POS_SINK	(ushort)(0x7FFF)
/* only upper 15 bins of position are used */
#define PIX_POS_MASK	(ushort)(0x7FFF)
/* lowest bin is flag for a masked position, i.e. all negative positions are masked */
#define PIX_FLG_MASK	(0x8000)
/* get mask for a given position */
#define PIX_MASKED(p)	((bool)((p & PIX_FLG_MASK)>>15))

#define POS_MASK	 0x0000FFFF00000000uL
#define POS_SHIFT	 32
#define SET_POS(s,x) (((s) & (~POS_MASK)) | ((((ulong)(x)) & 0xFFFFuL) << POS_SHIFT))
#define GET_POS(s)	 ((ushort)(((s) & POS_MASK) >> POS_SHIFT))

#define FLG_MASK	 0x0001000000000000uL
#define FLG_SHIFT	 48
#define SET_FLG(s,x) (((s) & (~FLG_MASK)) | ((((ulong)(x)) & 0x1uL) << FLG_SHIFT))
#define GET_FLG(s)	 ((bool)(((s) & FLG_MASK) >> FLG_SHIFT))

#define DTA_MASK	 0x00000000FFFFFFFFuL
#define DTA_SHIFT	 0
#define SET_DTA(s,x) (((s) & (~DTA_MASK)) | ((((ulong)VAL_TYPE_AS_ITYPE(x)) & 0xFFFFFFFFuL) << DTA_SHIFT))
#define GET_DTA(s)	 (ULONG_AS_VAL_TYPE(((s) & DTA_MASK) >> DTA_SHIFT))

#define NCS_MASK	 0x00000000FFFFFFFFuL
#define NCS_SHIFT	 0
#define SET_NCS(s,y) (((s) & (~NCS_MASK)) | ((((ulong)NCS_TYPE_AS_ITYPE(y)) & 0xFFFFFFFFuL) << NCS_SHIFT))
#define GET_NCS(s)	(ULONG_AS_NCS_TYPE(((s) & NCS_MASK) >> NCS_SHIFT))

/* Memory models for different board configurations */
#ifdef _USE_HBM_MEM_ /* using multiple HBM memories */
    #define ATTRIBUTE_MEN_BANK0 __attribute((buffer_location("HBM0")))
    #define ATTRIBUTE_MEN_BANK1 __attribute((buffer_location("HBM1")))
    #define ATTRIBUTE_MEN_BANK2 __attribute((buffer_location("HBM2")))
    #define ATTRIBUTE_MEN_BANK3 __attribute((buffer_location("HBM3")))
    #define ATTRIBUTE_MEN_BANK4 __attribute((buffer_location("HBM4")))
    #define ATTRIBUTE_MEN_BANK5 __attribute((buffer_location("HBM5")))
    #define ATTRIBUTE_MEN_BANK6 __attribute((buffer_location("HBM6")))
    #define ATTRIBUTE_MEN_BANK7 __attribute((buffer_location("HBM7")))
#else                /* BLANK is default */
    #define ATTRIBUTE_MEN_BANK0
    #define ATTRIBUTE_MEN_BANK1
    #define ATTRIBUTE_MEN_BANK2
    #define ATTRIBUTE_MEN_BANK3
    #define ATTRIBUTE_MEN_BANK4
    #define ATTRIBUTE_MEN_BANK5
    #define ATTRIBUTE_MEN_BANK6
    #define ATTRIBUTE_MEN_BANK7
#endif

__kernel void bincount2(global ATTRIBUTE_MEN_BANK4 T_BINNING_VAL* restrict odata,
                        global ATTRIBUTE_MEN_BANK4 T_BINNING_NCS* restrict oncounts,
                        const unsigned short olen,
                        global ATTRIBUTE_MEN_BANK2 const T_DATA_VAL* restrict idata1,
                        global ATTRIBUTE_MEN_BANK3 const T_DATA_VAL* restrict idata2,
                        const unsigned int idatalen,	/* note: likely we do not need this */
                        global ATTRIBUTE_MEN_BANK4 const unsigned int*   restrict ipixels,
                        global ATTRIBUTE_MEN_BANK5 const unsigned short* restrict ipositions,
                        global ATTRIBUTE_MEN_BANK6 const T_BINNING_VAL* restrict iweights,
                        global ATTRIBUTE_MEN_BANK7 const T_BINNING_VAL* restrict icorrections,
                        const unsigned int ilen,
                        global ATTRIBUTE_MEN_BANK4 unsigned long *restrict timing)
{
	const ulong2 s0 = {SET_POS(0uL, PIX_POS_SINK), 0uL};

	// local memory
	T_BINNING_VAL __attribute__((numbanks(BIN_RANK_PIX),
								bankwidth(sizeof(T_BINNING_VAL)),
								singlepump,
								max_concurrency(1),
								numreadports(1),
								numwriteports(1))) lmem_val[NBINS][BIN_RANK_PIX];

	T_BINNING_NCS __attribute__((numbanks(BIN_RANK_PIX),
								bankwidth(sizeof(T_BINNING_NCS)),
								singlepump,
								max_concurrency(1),
								numreadports(1),
								numwriteports(1))) lmem_ncs[NBINS][BIN_RANK_PIX];

	// bisort & resort local memory structures
	ulong2 __attribute__((register)) input_mem[BIN_RANK_BISORT][UNSEQ_LEN][BN_N] = {{{ s0 }}};
	ulong2 __attribute__((register)) bisort_mem[BIN_RANK_BISORT][BN_LEN+1][BN_N] = {{{ s0 }}};
	ulong2 __attribute__((register)) resort_mem[BIN_RANK_BISORT][UNSEQ_LEN][BN_N] = {{{ s0 }}};
	ulong2 __attribute__((register)) output_mem[BIN_RANK_BISORT][UNSEQ_LEN][BN_N] = {{{ s0 }}};
	ulong2 __attribute__((register)) output_last[BIN_RANK_BISORT] = { s0 };
	
	// initialize local memory
	#pragma ii 1
	for(unsigned i=0; i<NBINS; i++) {
		#pragma unroll
		for(uchar ipix=0; ipix<BIN_RANK_PIX; ipix++) {
			lmem_val[i][ipix] = (T_BINNING_VAL)0;
			lmem_ncs[i][ipix] = (T_BINNING_NCS)0;
		}
	}

	// Step 1: Declare multiple copies of variables acc, pos and last
	T_BINNING_VAL	__attribute__((register)) acc_val_copies[RELAX_LEN][BIN_RANK_PIX];
	T_BINNING_NCS	__attribute__((register)) acc_ncs_copies[RELAX_LEN][BIN_RANK_PIX];
	ushort			__attribute__((register)) pos_copies[RELAX_LEN][BIN_RANK_PIX];
 	bool			__attribute__((register)) flg_copies[RELAX_LEN][BIN_RANK_PIX];

	// Step 2: Initialize all copies
	for(unsigned i=0; i<RELAX_LEN; i++) {
		#pragma unroll
		for(uchar ipix=0; ipix<BIN_RANK_PIX; ipix++) {
			acc_val_copies[i][ipix] = (T_BINNING_VAL)0;
			acc_ncs_copies[i][ipix] = (T_BINNING_NCS)0;
			pos_copies[i][ipix] = PIX_POS_SINK;
			flg_copies[i][ipix] = false;
		}
	}

	unsigned int iplpix = (unsigned int)(-1);
    T_DATA_VAL __attribute__((register)) ldval_mem[BIN_RANK_PIX] = {0};

	// il ... index loaded control data
    // iplpix ... preloaded pixel number

	short counter = 0;

	for(unsigned int imain=0, il = 0; imain<(ilen+RELAX_LEN+BN_LEN+2*UNSEQ_LEN+10); imain++, il++) {
   
		// add bisort & resort output to lmem			  
		#pragma unroll
		for(short ipix=0; ipix<BIN_RANK_PIX; ipix++) {
			const short icol = (counter-1-(ipix % (BIN_RANK_PIX/BIN_RANK_BISORT))+2*UNSEQ_LEN) % UNSEQ_LEN; // result must be positive // 8
			//const short icol = (counter-2-(ipix % (BIN_RANK_PIX/BIN_RANK_BISORT))+2*UNSEQ_LEN) % UNSEQ_LEN; // result must be positive // 4
			const short ibis = ipix / (BIN_RANK_PIX/BIN_RANK_BISORT);
			const ulong2 sout = (icol==(UNSEQ_LEN-1)) ? output_last[ibis] : output_mem[ibis][icol+1][icol];
			// ---------------------------------------------------------------------------------------------------------------
			T_BINNING_VAL val = GET_DTA(sout.x);
			T_BINNING_NCS ncs = GET_NCS(sout.y);
			ushort pos = GET_POS(sout.x);
			bool flg = GET_FLG(sout.x);
			T_BINNING_VAL mem_val = (pos!=PIX_POS_SINK) ? lmem_val[pos % NBINS][ipix] : (T_BINNING_VAL)0;
			T_BINNING_VAL mem_ncs = (pos!=PIX_POS_SINK) ? lmem_ncs[pos % NBINS][ipix] : (T_BINNING_NCS)0;

			// Step 3: Use the last result
			ushort spos = pos_copies[RELAX_LEN-1][ipix];
			if(flg_copies[RELAX_LEN-1][ipix] && (spos!=PIX_POS_SINK)) {
				lmem_val[spos % NBINS][ipix] = acc_val_copies[RELAX_LEN-1][ipix];
				lmem_ncs[spos % NBINS][ipix] = acc_ncs_copies[RELAX_LEN-1][ipix];
			}
			T_BINNING_VAL acc_val = mem_val + val;
			T_BINNING_NCS acc_ncs = mem_ncs + ncs;

			// Step 4a: Shift copies
			#pragma unroll 
			for (unsigned j = RELAX_LEN-1; j > 0; j--) {
				acc_val_copies[j][ipix] = acc_val_copies[j-1][ipix];
				acc_ncs_copies[j][ipix] = acc_ncs_copies[j-1][ipix];
				pos_copies[j][ipix] = pos_copies[j-1][ipix];
				flg_copies[j][ipix] = flg_copies[j-1][ipix];
			}

			// Step 4b: Insert updated copy at the beginning
			acc_val_copies[0][ipix] = acc_val;
			acc_ncs_copies[0][ipix] = acc_ncs;
			pos_copies[0][ipix] = pos;
			flg_copies[0][ipix] = flg;
			// ---------------------------------------------------------------------------------------------------------------
			#ifdef _DEBUG_BOUT_
			printf("BISORT: imain=%d, counter=%d, ipix=%d, sout.dta = %d(%d)\n", imain, counter, ipix, (int)floor(GET_DTA(sout.x)), GET_FLG(sout.x));
			#endif
		}

		#pragma unroll
		for(short ibis=0; ibis<BIN_RANK_BISORT; ibis++) {

			/* --- last output memory --- */
			output_last[ibis] = output_mem[ibis][UNSEQ_LEN-1][UNSEQ_LEN-1];

			/* --- shift resorted output_memory --- */
			#pragma unroll
			for(short row=UNSEQ_LEN-2; row>=0; --row) {
				#pragma unroll
				for(short col=0; col<BN_N; ++col) {
					output_mem[ibis][row+1][col] = output_mem[ibis][row][col];
				}
			}

			/* ---- copy resort result --- */
			#pragma unroll
			for(short col=0; col<BN_N; ++col) {
				output_mem[ibis][0][col] = resort_mem[ibis][UNSEQ_LEN-1][col];
			}

			/* --- run resort --- */
			#pragma unroll
			for(short row=(UNSEQ_LEN-2); row>=0; --row) {
				const short isort = (UNSEQ_LEN-1) - row;
				const short ref = row + 1; // reference is already shifted
				// --- find remote ---
				const ushort ref_key = GET_POS(output_mem[ibis][ref][isort].x);
				const bool ref_flg = GET_FLG(output_mem[ibis][ref][isort].x);
				short iremote = BN_N;
				#pragma unroll
				for(short col=0; col<UNSEQ_LEN; ++col) {
					if(ref_flg && GET_FLG(resort_mem[ibis][row][col].x) && (col<isort) && (ref_key==GET_POS(resort_mem[ibis][row][col].x))) {
						iremote = col;
						#ifdef _DEBUG_BRESORT_
						printf("BISORT: col=%d is remote: key=%d(%d), isort=%d\n", col, GET_POS(resort_mem[ibis][row][col].x), GET_FLG(resort_mem[ibis][row][col].x), isort);
						#endif
					}
				}
				// --- shift & swap ---
				#pragma unroll
				for(short col=0; col<BN_N; ++col) {
					if(col==isort)
						if(iremote!=BN_N) {
							resort_mem[ibis][row+1][isort] = resort_mem[ibis][row][iremote];
							resort_mem[ibis][row+1][iremote] = resort_mem[ibis][row][isort];
						} else
							resort_mem[ibis][row+1][isort] = resort_mem[ibis][row][isort];
					else if(col!=iremote)
						resort_mem[ibis][row+1][col] = resort_mem[ibis][row][col];
				}
			} // row

			/* ---- copy bisort result --- */
			#pragma unroll
			for(short col=0; col<BN_N; ++col) {
				resort_mem[ibis][0][col] = bisort_mem[ibis][BN_LEN][col];
			}

			/* --- run bitonic merge sort network */
			#pragma unroll
			for(short row=BN_LEN-1; row>=0; --row) { // program
				#pragma unroll
				for(short col=0; col<BN_N; ++col) {
					// read the program
					const bool ishigh = (bool)_bp[row][col][0];
					if(ishigh) { // only one (high) needs to do an action
						const short remote = _bp[row][col][1];
						ulong2 slow, shigh;
						ushort xlow, xhigh;
						bool flow, fhigh;
						shigh = bisort_mem[ibis][row][col];
						slow  = bisort_mem[ibis][row][remote];
						xlow = GET_POS(slow.x);
						xhigh = GET_POS(shigh.x);
						flow = GET_FLG(slow.x);
						fhigh = GET_FLG(shigh.x);
						if((xlow > xhigh) || ((xlow == xhigh) && (!flow && fhigh))) { // swap
							bisort_mem[ibis][row+1][col] = bisort_mem[ibis][row][remote];
							bisort_mem[ibis][row+1][remote] = bisort_mem[ibis][row][col];
						} else if((xlow == xhigh) && flow && fhigh)
						{ // accumulation (into slow)
							T_BINNING_NCS nlow = GET_NCS(slow.y);
							T_BINNING_VAL dlow = GET_DTA(slow.x);
							nlow += GET_NCS(shigh.y);
							dlow += GET_DTA(shigh.x);
							bisort_mem[ibis][row+1][col] = (ulong2)(SET_POS(0uL, xlow) , 0uL);
							bisort_mem[ibis][row+1][remote] = (ulong2)(SET_POS(0uL, xlow) | SET_FLG(0uL, true) | SET_DTA(0uL, dlow), SET_NCS(0uL, nlow));
						} else {
							bisort_mem[ibis][row+1][col] = bisort_mem[ibis][row][col];
							bisort_mem[ibis][row+1][remote] = bisort_mem[ibis][row][remote];							
						}
					}
				} // cols, end of program
			} // rows

			/* --- update bisort from input --- */
			#pragma unroll
			for(short col=0; col<BN_N; ++col) {
				bisort_mem[ibis][0][col] = input_mem[ibis][UNSEQ_LEN-1][col];
			}
	
			/* --- shift input --- */
			#pragma unroll
			for(short row=(UNSEQ_LEN-2); row>=0; --row) {			
				#pragma unroll
				for(short col=0; col<=BN_N; ++col) {
					if(col<=row)
						input_mem[ibis][row+1][col] = input_mem[ibis][row][col];
				}
			}

		} // ibis (output, resort, bisort, input)

		// input		
		// get new input
		bool bload = (imain<ilen);
		// load control data
		const unsigned int  ilpix = (bload) ? ipixels[il] : 0;
		const short         ilpos = (bload) ? ipositions[il] : PIX_POS_SINK;
		const T_BINNING_VAL ilwgt = (bload) ? iweights[il] : (T_BINNING_VAL)0;
		const T_BINNING_VAL ilcor = (bload) ? icorrections[il] : (T_BINNING_VAL)0;
		const T_BINNING_VAL factor = ilwgt * ilcor; // multiplication
		#pragma unroll
		for(short ipix=0; ipix<BIN_RANK_PIX; ipix++) {
			const short icol = (counter-(ipix % (BIN_RANK_PIX/BIN_RANK_BISORT))+UNSEQ_LEN) % UNSEQ_LEN; // result must be positive
			const short ibis = ipix / (BIN_RANK_PIX/BIN_RANK_BISORT);
			if(bload) {
				// (conditional) load data and prepare them for processing
				T_DATA_VAL ldval;
				if(ilpix == iplpix)
					ldval = ldval_mem[ipix];
				else {
					ldval = (ipix<(BIN_RANK_PIX/2)) ? idata1[ilpix*(BIN_RANK_PIX/2)+ipix] : idata2[ilpix*(BIN_RANK_PIX/2)+ipix % (BIN_RANK_PIX/2)];
					ldval_mem[ipix] = ldval;
				}
            	const T_BINNING_VAL val = ldval * factor; // conversion and multiplication
				ulong2 sin;
				sin.x = SET_POS(0uL, ilpos & PIX_POS_MASK) | SET_FLG(0uL, ~PIX_MASKED(ilpos)) | SET_DTA(0uL, val);
				sin.y = SET_NCS(0uL, (T_BINNING_NCS)ilwgt);
				// set data
				input_mem[ibis][icol][icol] = sin;
			} else {
				// fill in pipeline with dummy data
				input_mem[ibis][icol][icol] = s0;
			}
			#ifdef _DEBUG_BIN_
			#pragma unroll		
			for(short col=0; col<BN_N; ++col)
				printf("BISORT: imain=%d, counter=%d, ipix=%d, input_mem[%d][%d][%d].dta = %d(%d)\n", imain, counter, ipix, ibis, icol, col, (int)floor(GET_DTA(input_mem[ibis][icol][col].x)), GET_FLG(input_mem[ibis][icol][col].x));
			#endif
		}
		iplpix = ilpix;
		
		// --- fill input with dummy data out of input cycles --
		#pragma unroll
		for(short ibis=0; ibis<BIN_RANK_BISORT; ibis++) {
			#pragma unroll
			for(short ipix=(BIN_RANK_PIX/BIN_RANK_BISORT); ipix<UNSEQ_LEN; ipix++) {
				const short icol = (counter-ipix+UNSEQ_LEN) % UNSEQ_LEN; // result must be positive
				input_mem[ibis][icol][icol] = s0;
			}
		}

		#ifdef _DEBUG_BRESMEM_
		#pragma unroll
		for(short ibis=0; ibis<BIN_RANK_BISORT; ibis++) {
			printf("BISORT-resort memory: ibis=%d:\n", ibis);
			#pragma unroll
			for(short row=UNSEQ_LEN-1; row>=0; --row) {
				printf("o[%02d] ", row);
				#pragma unroll
				for(short col=0; col<UNSEQ_LEN; ++col) {
					printf(" %d", GET_POS(output_mem[ibis][row][col].x));
					if (GET_FLG(output_mem[ibis][row][col].x))
						printf(" ");
					else
						printf("*");
				}
				printf("\n");
			}
			#pragma unroll
			for(short row=(UNSEQ_LEN-1); row>=0; --row) {
				printf("r[%02d] ", row);
				#pragma unroll
				for(short col=0; col<UNSEQ_LEN; ++col) {
					printf(" %d", GET_POS(resort_mem[ibis][row][col].x));
					if (GET_FLG(resort_mem[ibis][row][col].x))
						printf(" ");
					else
						printf("*");
				}
				printf("\n");
			}
			#pragma unroll
			for(short row=BN_LEN; row>=0; --row) {
				printf("b[%02d] ", row);
				#pragma unroll
				for(short col=0; col<UNSEQ_LEN; ++col) {
					printf(" %d", GET_POS(bisort_mem[ibis][row][col].x));
					if (GET_FLG(bisort_mem[ibis][row][col].x))
						printf(" ");
					else
						printf("*");
				}
				printf("\n");
			}
			#pragma unroll
			for(short row=(UNSEQ_LEN-1); row>=0; --row) {
				printf("i[%02d] ", row);
				#pragma unroll
				for(short col=0; col<UNSEQ_LEN; ++col) {
					printf(" %d", GET_POS(input_mem[ibis][row][col].x));
					if (GET_FLG(input_mem[ibis][row][col].x))
						printf(" ");
					else
						printf("*");
				}
				printf("\n");
			}
		}
		#endif
		
		/* increase counter */
		counter = (counter+1) % UNSEQ_LEN;

		//mem_fence(CLK_CHANNEL_MEM_FENCE); // without this II=1

	} // main-for-cycle

	// Step 5: Perform reduction on copies and save result
	#pragma ii 1
	for(unsigned short ibin=0; ibin<(olen % (NBINS+1)); ibin++) {
		#pragma unroll
		for(uchar ipix=0; ipix<BIN_RANK_PIX; ipix++) {
			odata[ibin*BIN_RANK_PIX+ipix] = lmem_val[ibin][ipix];
			oncounts[ibin*BIN_RANK_PIX+ipix] = lmem_ncs[ibin][ipix];
		}
	}
	
	// timing info
	timing[0] = 0; timing[1] = 0;
}

