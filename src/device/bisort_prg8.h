#ifndef __BISORT_PRG8_H__
#define __BISORT_PRG8_H__

/* -- length and depth of bitonic mergesort network ---------- 
   
   A bitonic mergesort network for N inputs where N = 2**n
   has depth = N/2 and len = n*(n+1)/2

   An array defined in this file describes the program for
   all cells/processors: [istep][iproc][{role,remote}]
  
  ----------------------------------------------------------*/
#define BISORT_NET8_N      8
#define BISORT_NET8_LEN    6
#define BISORT_NET8_DEPTH  4

/* bitonic mergesort program */
__constant unsigned short _bisort_prg8[BISORT_NET8_LEN][BISORT_NET8_N][2] =
      {{{0, 1},
        {1, 0},
        {1, 3},
        {0, 2},
        {0, 5},
        {1, 4},
        {1, 7},
        {0, 6}},

       {{0, 2},
        {0, 3},
        {1, 0},
        {1, 1},
        {1, 6},
        {1, 7},
        {0, 4},
        {0, 5}},

       {{0, 1},
        {1, 0},
        {0, 3},
        {1, 2},
        {1, 5},
        {0, 4},
        {1, 7},
        {0, 6}},

       {{0, 4},
        {0, 5},
        {0, 6},
        {0, 7},
        {1, 0},
        {1, 1},
        {1, 2},
        {1, 3}},

       {{0, 2},
        {0, 3},
        {1, 0},
        {1, 1},
        {0, 6},
        {0, 7},
        {1, 4},
        {1, 5}},

       {{0, 1},
        {1, 0},
        {0, 3},
        {1, 2},
        {0, 5},
        {1, 4},
        {0, 7},
        {1, 6}}};

#endif /* __BISORT_PRG8_H__ */

