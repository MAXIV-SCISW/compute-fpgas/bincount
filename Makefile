PROJECT=.
BUILD_DIR=$(PROJECT)/build

build-dir:
	mkdir -p $(PROJECT)/build/$(ARGS)
	cp -R $(PROJECT)/src $(PROJECT)/build/$(ARGS)
	@echo "=================================================================="
	@echo "BUILD DIR:" $(abspath $(PROJECT)/build/$(ARGS))
	@echo "=================================================================="

#@ include targets: s10-em, s10-em-legacy, bw-520-mx
include $(PROJECT)/boards/intel-s10/Makefile

#@ include targets: em, em-legacy, bw-385a, pac_a10
include $(PROJECT)/boards/intel-a10/Makefile

